program fillingData
    implicit none
    
    ! DECLARING VARIABLES
    double precision :: f, mult, x, pi=3.141592653589793238462643383279502884197
    double precision, dimension(5,2):: dat, dat2
    integer:: i, j  

    ! OPENING AND FILLING FILE "genData"
    open(unit=10,file="genData")
    dat(1,1) = 0.0
    dat(1,2) = 0.0 
    f = 5.0*sin(pi/4.0) + (pi/4.0)*cos(pi/4.0)
    mult = pi/8
    do i=1,4
        dat(i+1,1) = mult
        dat(i+1,2) = dat(i+1,1)*sin(dat(i+1,1))         
        mult = mult + pi/8
    end do

    do i=1,5
        write(10,*)(dat(i,j), j=1,2)  
    end do 
    close(10)

    !OPENING AND FILLING FILE "errorData"
    open(unit=1,file="errorData")
    dat2(1,1) = 0.0
    dat2(1,2) = 0.0
    mult = pi/16
    f = 5.0*sin(pi/4.0) + (pi/4.0)*cos(pi/4.0)
    do i=1,4
        dat2(i+1,1) = mult
        x = mult
        dat2(i+1,2) = x*(x-dat(2,1))*(x-dat(3,1))*(x-dat(4,1))*(x-dat(5,1))*f/120.0
        mult = mult + pi/16
    end do

    do i=1,5
        write(1,*)(dat2(i,j), j=1,2)  
    end do 
    close(1)
    
end program fillingData