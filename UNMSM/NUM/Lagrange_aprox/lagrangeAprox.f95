program lagrangeAprox
    implicit none

    ! VARIABLE DECLARATION
    integer:: i,j,n=0 
    real, allocatable:: dataset(:,:), lagMult(:,:)
    real:: l, x, y=0
    
    ! READING FILE
    open(unit=1,file="data.txt")
    do 
        read(1,*, end=10) 
        n = n + 1 
    end do
    10 close(1)

    open(unit=1,file="data.txt")
    
    allocate(dataset(n,2))
    allocate(lagMult(n,1))

    do i=1,n 
        read(1,*) dataset(i,1), dataset(i,2)
    end do 
    
    close(1)

    ! READING INTERPOLATING REQUIRED VALUE X
    print*, "Ingrese el valor de X:"
    read*, x

    ! CALCULATING LAGRANGE MULTIPLICATORS
    do i=1,n 
        l=1.0
        do j=1,n 
            if(i/=j) then
                l = l*((x-dataset(j,1))/(dataset(i,1)-dataset(j,1)))
            end if
        end do 
        lagMult(i,1) = l 
    end do 
     
    ! INTERPOLATE THE REQUIRED VALUE OF Y FOR X
    do i=1,n
        y = y + lagMult(i,1)*dataset(i,2)
    end do 
    
    print*, "El valor interpolado de y para x = ", x, " es: ", y

end program lagrangeAprox