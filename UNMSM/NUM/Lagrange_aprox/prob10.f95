program fillingData
    implicit none
    
    ! DECLARING VARIABLES
    double precision :: f, mult, x
    double precision, dimension(6,2):: dat
    double precision, dimension(21,2):: dat2
    integer:: i, j  

    ! OPENING AND FILLING FILE "genData"
    open(unit=10,file="genData")
    dat(1,1) = 0.0
    dat(1,2) = 0.0 
    mult = 0.4
    do i=1,5
        dat(i+1,1) = mult
        dat(i+1,2) = sqrt(mult)*cos(mult)     
        mult = mult + 0.4
    end do

    do i=1,6
        write(10,*)(dat(i,j), j=1,2)  
    end do 
    close(10)

    !OPENING AND FILLING FILE "errorData"
    open(unit=1,file="errorData")
    dat2(1,1) = 0.0
    dat2(1,2) = 0.0
    mult = 0.1
    f = (-64*cos(1.0)-192*sin(1.0)-240*cos(1.0)+480*sin(1.0)+900*cos(1.0)-1260*sin(1.0)-945*cos(1.0))/64.0
    do i=1,20
        dat2(i+1,1) = mult
        x = mult
        dat2(i+1,2) = x*(x-dat(2,1))*(x-dat(3,1))*(x-dat(4,1))*(x-dat(5,1)*(x-dat(6,1)))*f/720.0
        mult = mult + 0.1
    end do

    do i=1,21
        write(1,*)(dat2(i,j), j=1,2)  
    end do 
    close(1)
end program fillingData