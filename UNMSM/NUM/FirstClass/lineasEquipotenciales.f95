program lineasEquipotenciales 
    implicit none
    
    !Declarando contadores
    integer:: a = 0, i, j
    !Declarando arreglo de datos y funciones
    real, dimension(100000,3):: data
    real:: potentialCalc
    !Declarando variables de entrada
    real:: q1, q2, d

    !Leyendo variables de entrada
    print*, "Ingrese valor de la carga 1"
    read*, q1
    print*, "Ingrese valor de la carga 2"
    read*, q2
    print*, "Ingrese valor de la distancia 'd'"
    read*, d 

    !Llenando el array
    do i=-100,100
        do j=-100,100
            if(i==0) then
                if(j/=d .and. j/=-d) then
                    a = a+1
                    data(a,1) = real(i)/1000
                    data(a,2) = real(j)/1000
                    data(a,3) = potentialCalc(q1, q2, sqrt(i**2 + (j-d)**2), sqrt(i**2 + (j+d)**2))
                end if
            else
                a = a+1
                data(a,1) = real(i)/1000
                data(a,2) = real(j)/1000
                data(a,3) = potentialCalc(q1, q2, sqrt(i**2 + (j-d)**2), sqrt(i**2 + (j+d)**2))
            end if
        end do
    end do
    print*, a
    !Llenando el archivo
    open(7,file="dataset.txt")
    do i=1,a
        write(7,'(f8.3, f8.3, f15.3)')(data(i,j), j=1,3)
    end do
    close(7)

end program lineasEquipotenciales 

real function potentialCalc(q1, q2, r1, r2)

    !Definiendo constante
    real, parameter:: k = 9.0*10.0**9

    !Calculando potencial
    potentialCalc = k*q1/r1 + k*q2/r2

end