program potencialGeneradoPorCargas
implicit none

! DECLARANDO DATOS
real:: q1, q2, d, potential, calculateY1, calculateY2, x, y, v
integer:: i, j, n, a=0
real, allocatable, dimension (:) :: data 
real, dimension(7200,3):: dataset1, dataset2, dataset3, array1, array2
real, Parameter:: k=9.0*10.0**(9.0), pi=3.1415

! LEYENDO DATOS DE ENTRADA
print*, "Ingrese la carga q1"
read*, q1
print*, "Ingrese la carga q2"
read*, q2
print*, "Distancia d"
read*, d 


do i=1,20
    v = k*q1/real(i)
    do j=1,360
        a = a+1
        x = i*cos(real(j)*2.0*pi/360.0)
        y = i*sin(real(j)*2.0*pi/360.0)+d/2.0
        dataset1(a,1) = x 
        dataset1(a,2) = y
        dataset1(a,3) = v
    end do

end do

a=0

do i=1,20
    v = k*q2/real(i)
    do j=1,360
        a = a+1
        x = i*cos(real(j)*2.0*pi/360.0)
        y = i*sin(real(j)*2.0*pi/360.0)-d/2.0
        dataset2(a,1) = x 
        dataset2(a,2) = y
        dataset2(a,3) = v
    end do

end do

open(unit = 1)
open(unit = 7)

do i=1,7200
    write(1,'(f6.2, f6.2, f20.2)')(dataset1(i,j),j=1,3)
end do

do i=1,7200
    write(7,'(f6.2, f6.2, f20.2)')(dataset2(i,j),j=1,3)
end do

close(unit = 1)
close(unit = 7)

open(unit=1, file='fort.1')

do i=1,7200
    read(1,*) array1(i,1), array1(i,2), array1(i,3)
end do 

close(1)

open(unit=7, file='fort.7')

do i=1,7200
    read(2,*) array2(i,1), array2(i,2), array2(i,3)
end do 

close(7)

a=1

do i=1,7200
    do j=1,7200
        if(array1(i,1) == array2(j,1) .and. array1(i,2) == array2(j,2)) then
            print*, "Hola"
            dataset3(a,1) = array1(i,1)
            dataset3(a,2) = array1(i,2)
            dataset3(a,3) = array1(i,3)+array2(j,3)
            a = a+1
        end if
    end do
end do



do i=1,7200
    write(8,'(f6.2, f6.2, f20.2)')(dataset3(i,j),j=1,3)
end do

close(unit=7)

! print*, "Ingrese la distancia 'd'"
! read*, d

! print*, "Ingrese el número deseado de líneas equipotenciales"
! read*, n 

! allocate(data(n))
! do i=1,n
    ! print*, "Ingrese el valor del potencial en la línea ", i 
    ! read*, data(i)
! end do


! CALCULANDO EL POTENCIAL
! open(unit = 7)
! do i = 1,1000,2
        ! dataset(i,1) = real(i)*1000.0 
        ! dataset(i,2) = calculateY1(q1,q2,data(i),real(i)*1000.0)
        ! dataset(i+1,1) = real(i)*1000.0
        ! dataset(i+1,2) = calculateY2(q1,q2,data(i),real(i)*1000.0)
! end do

! do i = 1,10000
! write(7,*)(dataset(i,j),j=1,3)
! end do

! v = potential(q1, q2, d, x, y)
! print*, "El potencial es ", v, " V/m" 

end program potencialGeneradoPorCargas

real function potential(q1, q2, d, x, y)

    ! DECLARANDO CARGAS Y CAMPOS
    real:: q1, q2, E1, E2, r1, r2, d, x, distanceR1, distanceR2
    real, Parameter:: k=9.0*10.0**(9.0)

    ! CALCULANDO LAS DISTANCIAS
    r1 = distanceR1(d, x, y)
    r2 = distanceR2(d, x, y)

    ! CALCULANDO LOS CAMPOS
    E1 = k*q1/r1**2
    E2 = k*q2/r2**2

    ! CALCULANDO EL POTENCIAL
    potential = r1*E1 + r2*E2
    return

end 

real function distanceR1(d, x, y)

    ! DECLARANDO DISTANCIAS
    real:: x, y   

    ! CALCULANDO LA POSICIÓN DE LAS CARGAS
    distanceR1 = sqrt(x**2 + (d/2.0-y)**2)
    return

end 

real function distanceR2(d, x, y)

    ! DECLARANDO DISTANCIAS
    real:: x, y   

    ! CALCULANDO LA POSICIÓN DE LAS CARGAS
    distanceR2 = sqrt(x**2 + (d/2.0+y)**2)
    return

end 

real function calculateY1(q1,q2,v,x)
    real, Parameter:: k=9.0*10.0**(9.0)
    real:: q1,q2,v,x 

    calculateY1 = sqrt(((k*q1+k*q2)/v)**2+x**2)
    return
end 

real function calculateY2(q1,q2,v,x)
    real, Parameter:: k=9.0*10.0**(9.0)
    real:: q1,q2,v,x 

    calculateY2 = -sqrt(((k*q1+k*q2)/v)**2+x**2)
    return
end 