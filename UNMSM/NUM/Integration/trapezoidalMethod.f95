program integrationAlgorithm
    implicit none

    ! DECLARING VARIABLES
    real:: a,b,y,tm,sr, PI=4.D0*DATAN(1.D0)

    integer:: n, e

    ! READING INTEGRATION LIMITS
    print*, "Ingrese el límite de integración inferior"
    read*, a
    ! a = 0.0
    print*, "Ingrese el límite de integración superior"
    read*, b
    !b = 2.0
    !b = PI/2.0
    print*, "Ingrese el número de secciones para aproximar la función"
    read*, n
    print*, "Ingrese 1 si el método es del trapecio o 2 si es de Simpson"
    read*, e
    !e = 2
    ! INTEGRATING FUNCTION
    if(e==1) then
        y = tm(a,b,n)
    else if(e==2) then
        y = sr(a,b,n)
    end if
    print*, "La integral definida de la función es: ", y

end program integrationAlgorithm

real function f(x)

    ! DECLARING INPUT VARIABLE
    real:: x 

    ! OPERATION
    f = (5400.00*x)/(8.27*x**2+2000.0)
    return

end 

! TRAPEZOIDAL METHOD
real function tm(a,b,n)

    ! DECLARING VARIABLES
    real:: a,b,f,h 
    integer:: j,n 

    ! OPERATIONS
    h = (b-a)/real(n)
    tm = 0.5*h*f(a) + 0.5*h*f(b)
    do j=1,n-1
        tm = tm + h*f(a+real(j)*h)
    end do 
    return
end 

! SIMPSON'S RULE
real function sr(a,b,n)

    ! DECLARING VARIABLES
    real:: a,b,f,h 
    integer:: j,n 

    ! OPERATIONS
    h = (b-a)/real(n)
    sr = (1.0/3.0)*h*f(a) + (1.0/3.0)*h*f(b)
    do j=1,n-1,2
        sr = sr + (4.0/3.0)*h*f(a+real(j)*h)
    end do 
    do j=2,n-2,2
        sr = sr + (2.0/3.0)*h*f(a+real(j)*h)
    end do 
    return
end 