program displacementCalculator 
    implicit none

    ! DECLARING VARIABLES
    real:: ta, tb, h, sum, integral
    real, dimension(0:8,0:1):: data
    integer:: n=8, i

    ! READING FILE
    open(unit=1, file='data.txt')
    do i=0,8
        read(1,*) data(i,0), data(i,1)
    end do 
    close(1)

    ! OPERATIONS
    ta = data(0,0)
    tb = data(n,0)
    h = (tb-ta)/(n)
    sum = 0.0
    do i=1,n
        sum = sum + data(i,1)
    end do 
    integral = 0.5*h*(data(0,1)+2.0*sum+data(n,1))

    ! PRINT DATA
    print*, integral
     
end program displacementCalculator  