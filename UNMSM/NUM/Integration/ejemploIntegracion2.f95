program positionCalculator
    implicit none

    ! DECLARING VARIABLES
    real:: ta, tb, tc, h, sum, integral1, integral2
    real, dimension(9,2):: data
    integer:: n=9, i

    ! READING FILE
    open(unit=1, file='data.txt')
    do i=1,9
        read(1,*) data(i,1), data(i,2)
    end do 
    close(1)

    ! OPERATIONS

    ! Calculationg the position for t=0.8
    ta = data(1,1)
    tb = data(9,1)
    h = (tb-ta)/(n-1)
    sum = 0.0
    do i=2,n-1
        sum = sum + data(i,2)
    end do 
    integral1 = 0.5*h*(data(1,2)+2.0*sum+data(n,2))

    ! Calculating the position for t=0.4
    tc = data(5,1)
    h = (tc-ta)/(4)
    sum = 0.0
    do i=2,4
        sum = sum + data(i,2)
    end do 
    integral2 = 0.5*h*(data(1,2)+2.0*sum+data(5,2))

    ! PRINT DATA
    print*, "En los últimos 0.4 seg. el desplazamiento fue de ", integral1 - integral2, "unidades" 

end program positionCalculator  