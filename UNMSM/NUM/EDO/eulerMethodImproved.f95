program eulerExample2
    implicit none

    ! VARIABLE DECLARATION
    real:: a,b,x,y,h, funct
    integer:: n,i

    ! INPUT DATA
    print*, "Insertar el número de pasos n: "
    read*, n 
    print*, "Indicar la condición inicial de x: "
    read*, a 
    print*, "Indicar el valor inicial de y(x): "
    read*, y
    print*, "¿Cual es el valor solicitado?: "
    read*, b 

    ! OPERATIONS
    x = a 
    h = (b-a)/n 
    do i = 0, n-1
        x = x+h 
        y = y + 0.5*h*(funct(x,y)+funct(x,y+h*funct(x,y)))        
    end do

    ! OUTPUT DATA
    print*, y
    
end program eulerExample2

real function funct(x,y)

    real:: x,y 
    funct =  -y**1.5+1
    return

end 