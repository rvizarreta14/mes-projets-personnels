program eulerExample2
    implicit none

    ! VARIABLE DECLARATION
    double precision:: a,b,x,y,h, funct
    integer:: n,i

    ! INPUT DATA
    ! print*, "Insertar el número de pasos n: "
    ! read*, n 
    ! print*, "Indicar la condición inicial de x: "
    ! read*, a 
    ! print*, "Indicar el valor inicial de y(x): "
    ! read*, y
    print*, "¿Cual es el valor solicitado?: "
    read*, b 

    a = 0.0
    y = 0.0
    n = 100

    ! OPERATIONS
    x = a 
    h = (b-a)/n 
    do i = 0, n-1
        x = x+h 
        y = y + 0.5*h*(funct(x,y)+funct(x,y+h*funct(x,y)))        
    end do

    ! OUTPUT DATA
    print*, y
    
end program eulerExample2

double precision function funct(x,y)

    double precision:: x,y, f0, c, p
    f0 = 10.0**(-9)
    c = 10.0**(-18)
    p = 10.0**(-2)
    funct = 2*f0*(exp(-c-c*p**2))*(sinh(2.0*c*p)-y*cosh(2.0*c*p))
    return

end 