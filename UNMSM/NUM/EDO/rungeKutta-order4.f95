program eulerExample2
    implicit none

    ! VARIABLE DECLARATION
    double precision:: a,b,x,y,h, funct, euler, k1, k2, k3, k4, array(1000,2)
    integer:: n,i,j

    ! INPUT DATA
    ! print*, "Insertar el número de pasos n: "
    ! read*, n 
    ! print*, "Indicar la condición inicial de x: "
    ! read*, a 
    ! print*, "Indicar el valor inicial de y(x): "
    ! read*, y
    ! print*, "¿Cual es el valor solicitado?: "
    ! read*, b 

    a = 0.0
    y = 100.0
    n = 100

    ! OPERATIONS

    do i=1,1000
        b = i
        x = a 
        h = (b-a)/n 
        do j = 0, n-1
            k1 = funct(x,y)
            k2 = funct(x+0.5*h,y+0.5*k1)
            k3 = funct(x+0.5*h,y+0.5*k2)
            k4 = funct(x+h,y+k3)
            x = x+h 
            y = y + h*((1./6.)*k1+(1./3.)*k2+(1./3.)*k3+(1./6.)*k4)      
        end do
        array(i,1) = x
        array(i,2) = y
    end do 

    open(unit=2,file="data.txt")
     
    do i = 1,1000
        write(2,*) (array(i,j), j=1,2)
    end do

    close(2)

    ! OUTPUT DATA
    print*, y
    
end program eulerExample2

double precision function funct(x,y)

    double precision:: x,y, f0, c, p
    f0 = 10.0**(-9)
    c = 10.0**(-18)
    p = 10.0**(-2)
    funct = 2*f0*(exp(-c-c*p**2))*(sinh(2.0*c*p)-y*cosh(2.0*c*p))
    return

end 