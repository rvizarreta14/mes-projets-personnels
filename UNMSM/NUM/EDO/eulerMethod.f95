program eulerExample
    implicit none
    ! VARIABLE DECLARATION
    real:: x,y,f,xf, h 
    integer:: n,i 

    ! INPUT DATA
    x = 0.0
    y = 10.0
    f = 0.0
    xf = 1.0
    print*, "Ingresar n"
    read*, n 

    ! OPERATIONS
    h = (xf-x)/n 
    do i=0,n-1
        x = x+h  
        f = -y**1.5 +1.0
        y = y+h*f 
    end do 

    ! OUTPUT DATA
    print*, y
    
end program eulerExample