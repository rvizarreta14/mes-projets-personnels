program pendulumExample
    implicit none
    ! VARIABLE DECLARATION
    real:: t0, x, y, t, h, y1, inp, array(1000,2)
    real, parameter:: g = 9.81, l = 2.0, pi=3.14159265359
    integer:: i, n, j

    ! INPUT DATA
    t0 = 0.0
    !x = 1.4
    !y = 0.0
    n = 1000
    !print*, "Ingrese el tiempo deseado (seg)"
    !read*, inp 
    !t = inp

    ! OPERATIONS

    do j=1,200
        t = j/10.0
        x = 2
        y = 0.0
        array(j,1) = t

        h = (t-t0)/n 
        do i=0,n-1
            t = t+h 
            !y1 = y + 0.5*h*(-(g/l)*sin(x)-(g/l)*sin(x))
            !x = x + 0.5*h*(y+y)
            y1 = y + 0.5*h*(-(g/l)*sin(x)-(g/l)*sin(x+h*y))
            x = x + 0.5*h*(y+y-h*(g/l)*sin(x))
            y = y1 
        end do 



        array(j,2) = x
    
    end do 

    open(unit=10, file="data.txt")
    do i=1,200
        write(10,*) (array(i,j), j=1,2)
    end do 

    ! OUTPUT DATA
    ! print*, "El ángulo de inclinación a un t =", inp, " seg es: ", x, "rad."

end program pendulumExample