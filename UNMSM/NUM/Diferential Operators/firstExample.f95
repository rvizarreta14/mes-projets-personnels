program differenceCalculator
    implicit none
    
    ! VARIABLE DECLARATION
    real, dimension(9,10):: dataset
    integer:: n=9, i, j

    ! FILLING ARRAY WITH 0's

    do i=1,9
        do j=1,10
            dataset(i,j) = 0.0
        end do
    end do

    ! READING FILE "data"
    open(unit=2, file="data")
    do i=1,9
        read(2,*) dataset(i,1), dataset(i,2)
    end do     
    close(2)

    ! FILLING ARRAY
    do j=3,10        
            do i=1,9
                if(i<n) then  
                    dataset(i,j) = dataset(i+1,j-1) - dataset(i,j-1)
                end if
            end do
            n = n-1      
    end do

    ! PRINTING THE ARRAY
    do i=1,9
        write(*,"(10(f5.1,2x))") (dataset(i,j), j=1,10-i+1)
    end do 
  
end program differenceCalculator