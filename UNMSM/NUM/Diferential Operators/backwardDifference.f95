program backwardDifference
    implicit none
    
    ! VARIABLE DECLARATION
    double precision, allocatable:: dataset(:,:)
    integer:: n=1, i, j, lines

    ! READING NUMBER OF ROWS IN FILE "data"
    open(unit=2, file="data")
    do
        read(2,*,end=10)
        lines = lines + 1
    end do
    10 close(2)

    ! ALLOCATING SPACE IN MEMORY FOR ARRAY
    allocate(dataset(lines,lines+1))

    ! READING FILE "data"
    open(unit=2, file="data")
    do i=1,lines
        read(2,*) dataset(i,1), dataset(i,2)
    end do     
    close(2)

    ! FILLING ARRAY
    do j=3,lines+1        
            do i=1,lines 
                if(i>n) then  
                    dataset(i,j) =  dataset(i,j-1) - dataset(i-1,j-1)
                end if
            end do
            n = n+1      
    end do

    ! PRINTING THE ARRAY
    do i=1,lines
        write(*,"(100(f8.5,2x))") (dataset(i,j), j=1,i+1)
    end do 
  
end program backwardDifference 