program gaussJordan
    implicit none
    
    ! DECLARING VARIABLES
    real, dimension(:,:), allocatable:: dataset
    integer:: i,j,k,l,n=0

    ! READING FILE
    open(unit=7, file="data")
    do 
        read(7,*,end=10)
        n = n+1
    end do 
    10 close(7)

    allocate(dataset(0:n-1,0:n))

    open(unit=7, file="data")
    do i = 0,n-1
        read(7,*) dataset(i,:)
        !write(*,"(100(f5.2, 1x))") dataset(i,:)
    end do 
    close(7)
  
    ! OPERATIONS
    do j=0,n-1
        do i=j,n-2
                if(dataset(j,j)/=1.0) then
                    do k=i+1,n-1
                        if (dataset(j,j) == 0.0) then
                            dataset(i,:) = dataset(i,:) + dataset(k,:)
                        else 
                            dataset(j,:) = dataset(j,:)/dataset(j,j)
                            exit 
                        end if 
                    end do 
                end if 
                dataset(i+1,:) = dataset(i+1,:) - dataset(j,:)*dataset(i+1,j)

        end do

        if(j==n-1) dataset(n-1,:) =  dataset(n-1,:)/dataset(n-1,n-1) 

        do l=j,1,-1
            dataset(l-1,:) = dataset(l-1,:) - dataset(j,:)*dataset(l-1,j)
        end do 

    end do 

    ! PRINTING MATRIX
    do i=0, n-1
        write(*,"(100(f7.2, 1x))") dataset(i,:)
    end do 

end program gaussJordan

