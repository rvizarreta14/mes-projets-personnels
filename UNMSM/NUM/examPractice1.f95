program velocityCalculator
    implicit none

    ! QUESTION 1 - FORWARD DIFFERENTE TABLE
    ! --------------------------------------

     
    ! VARIABLE DECLARATION
    double precision, allocatable:: dataset(:,:), coefMatrix(:)
    integer:: n=0, i, j, m !number of items
    double precision:: s, x, h, factorial, mult, sAcum, ta, tb, sum, integral

    ! READING NUMBER OF ROWS IN FILE "data"
    open(unit=2, file="data.txt")
    do
        read(2,*,end=10)
        n = n + 1
    end do
    10 close(2)
    m=n
    ! ALLOCATING SPACE IN MEMORY FOR ARRAY dataset
    allocate(dataset(n,n+1))

    ! READING FILE "data"
    open(unit=2, file="data.txt")
    do i=1,n
        read(2,*) dataset(i,1), dataset(i,2)
    end do     
    close(2)

    ! FILLING ARRAY dataset
    do j=3,n+1        
           do i=1,n
                if(i<n) then  
                    dataset(i,j) = dataset(i+1,j-1) - dataset(i,j-1)
                end if
            end do
            n = n-1      
    end do

    ! PRINTING THE ARRAY

    print*, "FORWARD DIFFERENCE TABLE:"
    print*, "-------------------------"

    do i=1,m
        write(*,"(100(f5.2,2x))") (dataset(i,j), j=1,m-i+2)
    end do 

    ! QUESTION 2 - INTERPOLATION FOR X=0.7
    ! --------------------------------------

    ! CALCULATING h
    h = dataset(2,1)-dataset(1,1) !0.1
    ! DETERMINATING THE COEFFICIENT MATRIX
    print*, "-------------------------"
    print*, "Ingrese el valor a calcular: "
    read*, x

    ! CALCULATING DE LOCAL COORDINATE s
    s = (x-dataset(1,1))/h !7

    ! ALLOCATING SPACE IN MEMORY FOR ARRAY coefMatrix
    allocate(coefMatrix(m+1))

    ! FILLING ARRAY coefMatrix
    do i=1,m
        coefMatrix(i) = (dataset(1,1+i)/factorial(i-1))
    end do

    ! CALCULATING THE REQUIRED VALUE
    mult = coefMatrix(1)
    sAcum = s
    do i=1, m
        mult = mult + coefMatrix(i+1)*sAcum
        sAcum = sAcum*(s-i)
    end do 

    print*, "FOR X = 0.7: F(X)=", mult
 
    ! QUESTION 3 - DISTANCE BETWEEN 0.0 AND 0.5 SEG
    ! ----------------------------------------------

    ! OPERATIONS
    ta = dataset(1,1)
    tb = dataset(6,1)
    sum = 0.0
    do i=2,5
        sum = sum + dataset(i,1)
    end do 
    integral = 0.5*h*(dataset(1,1)+2.0*sum+dataset(6,1))

    ! PRINT DATA
    print*, "-------------------------"
    print*, "LA DISTANCIA RECORRIDA EN 0.5 SEG ES: ", integral, "m"
    
end program velocityCalculator



double precision function factorial(n)
    integer:: n, i 
    factorial = 1.0

    if(n/=0) then
        do i=1,n
            factorial = factorial*real(i)
        end do    
    end if
end