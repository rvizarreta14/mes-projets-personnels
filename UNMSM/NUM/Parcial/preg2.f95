program positionCalculator
    implicit none
    
        ! VARIABLE DECLARATION
        real:: a,b,h,f,area    
        integer:: n, i
    
        ! READING INPUT DATA
        print*, "Ingrese el límite de integración inferior"
        read*, a
        print*, "Ingrese el límite de integración superior"
        read*, b

        ! OPERATIONS
        n = 100
        h = (b-a)/real(n)
        area = (1.0/3.0)*h*f(a) + (1.0/3.0)*h*f(b)
        do i=1,n-1,2
            area = area + (4.0/3.0)*h*f(a+real(i)*h)
        end do 
        do i=2,n-2,2
            area = area + (2.0/3.0)*h*f(a+real(i)*h)
        end do 

        ! OUTPUT DATA
        print*, "La distancia que recorre el auto es: ", area, "metros."
    
    end program positionCalculator
    
    real function f(x)    
        ! DECLARING INPUT VARIABLE
        real:: x     
        ! OPERATION
        f = (5400.00*x)/(8.276*x**2+2000.0)
        return    
    end 
    