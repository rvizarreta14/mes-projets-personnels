! a) Para la función f(x) dada, el rango de x en el cual
! aparece una división entre cero (el número más pequeño 3x10^-39)
! es cuando la tanhx se encuentra entre [min, min + epsilon), teniendo en 
! cuenta que: min = 3x10^-39 y epsilon = 1.2x10^-7.

! b) Haciendo el tratamiento algebraico, se obtiene que f(x) = 0.5(1+e^2x)