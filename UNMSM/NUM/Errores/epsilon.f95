program Ejercicio4_Errores

    implicit none

    real:: e1 = 1
    ! Se utiliza el atributo kind de un tipo de dato Real para la doble precisión,
    ! este garantiza por lo menos 15 cifras significativas
    integer, parameter :: dp = selected_real_kind(15, 307)
    real(kind=dp):: e2 = 1

    do while(e1+1>1) 
        print*, e1 
        e1 = e1/2
    end do

    do while(e2+1>1) 
        print*, e2 
        e2 = e2/2
    end do 

    print*, "Epsilon con precisión simple: ", e1
    print*, "Epsilon con doble precisión: ", e2
    
end program Ejercicio4_Errores