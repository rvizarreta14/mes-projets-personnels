program Ejercicio4_Errores

    implicit none

    ! Definiendo e1 como el Epsilon e1 con precisión simple
    real:: e1 = 1
    ! Se utiliza el atributo kind de un tipo de dato Real para la doble precisión,
    ! este garantiza por lo menos 15 cifras significativas
    integer, parameter :: dp = selected_real_kind(15, 307)
    ! Definiendo el Epsilon e2 con doble precisión
    real(kind=dp):: e2 = 1

    do while(e1+1>1) 
        e1 = e1/2
    end do

    do while(e2+1>1) 
        e2 = e2/2
    end do 

    print*, 1.0/(e2/2)

    print*, "Epsilon con precisión simple: ", e1 ! 5.96046448E-08
    print*, "Epsilon con doble precisión: ", e2 ! 1.1102230246251565E-016

    ! Se evidencia que utilizando la doble precisión se tiene un epsilon
    ! de la máquina más pequeño que con precisión simple. Esto es útil al
    ! trabajar con datos cuantitativamente muy cercanos a cero, de modo que
    ! un valor de punto flotante pequeño permite trabajar con intervalos más
    ! pequeños entre dos números reales consecutivos.
    
end program Ejercicio4_Errores