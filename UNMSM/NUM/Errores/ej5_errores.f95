program Ejercicio5_Errores
    
    implicit none

    ! Definiendo la suma y errores en precisión simple
    real:: sum = 0.0, ea, er
    integer:: i

    ! Se utiliza el atributo kind de un tipo de dato Real para la doble precisión,
    ! este garantiza por lo menos 15 cifras significativas
    integer, parameter :: dp = selected_real_kind(15, 307)
    ! Definiendo el Epsilon e2 con doble precisión
    real(kind=dp):: sum2 = 0.0, ea2, er2

    do i=1,1000000
        sum = sum + 0.000001
        sum2 = sum2 + 0.000001
    end do

    ! Calculando el error absoluto (ea) y relativo (er)
    ea = 1.0 - sum
    ea2 = 1.0 - sum2
    er = ea*100/1.0
    er2 = ea2*100/1.0
 
    print*, "La suma solicitada en precisión simple es igual a: ", sum ! 1.00903893
    print*, "El error absoluto en precisión simple es: ", ea ! -9.03892417E-03
    print*, "El error relativo en precisión simple es: ", er, "%" ! -0.903892517%

    print*, "La suma solicitada en precisión doble es igual a: ", sum2 ! 0.99999999747524271
    print*, "El error absoluto en precisión doble es: ", ea2 ! 2.5247572921216488E-009
    print*, "El error relativo en precisión doble es: ", er2, "%" ! % 2.5247572921216488E-007%

    ! Se evidencia que una estrategia para disminuir el error es utilizar 
    ! la doble precisión como tipo de dato. Esto debido al mayor número de cifras
    ! significativas.
    
end program Ejercicio5_Errores