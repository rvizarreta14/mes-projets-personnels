program Ejercicio6_Errores

    implicit none

    ! Se utiliza el atributo kind de un tipo de dato Real,
    ! este garantiza por lo menos 15 cifras significativas
    integer, parameter :: dp = selected_real_kind(15, 307)
    real(kind=dp):: x, sin
    real:: factorial
    integer:: i

    ! Ingreso de datos
    print*, "Ingresa el valor del ángulo cuyo seno desea calcular (en radianes)"
    read*, x

    ! Se aproxima el seno a la serie de Taylor
    
    sin = x    
    do i=2,10
        sin = sin +(-1)**(i+1)*(x**(2*i-1))/factorial(2*i-1)
        ! Se imprimen los valores del seno de x para cada valor de la serie
        ! de n = 2,3,4,5.
        print*, "Para n= ", i, " el seno de ", x, " es: ", sin
    end do 
    
end program Ejercicio6_Errores

real function factorial(n)

    integer:: n, i 
    factorial = 1.0

    if(n/=0) then
        do i=1,n
            factorial = factorial*real(i)
        end do    
    end if

end