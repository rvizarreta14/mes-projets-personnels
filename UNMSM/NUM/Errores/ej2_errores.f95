program Ejercicio2_Errores

    implicit none

    ! Se utiliza el atributo kind de un tipo de dato Real,
    ! este garantiza por lo menos 15 cifras significativas
    integer, parameter :: dp = selected_real_kind(15, 307)
    real:: x=0.0001, exp = 1.0
    real:: factorial
    integer:: i

    ! Se calculan los tres primeros términos de la serie de Taylor
    do i=1,3
        exp = exp + (x**i)/factorial(i)
    end do 

    print*, "El valor de e^x - 1 es: ", exp-1.0 ! 1.0000499764029058E-004
    
end program Ejercicio2_Errores

real function factorial(n)

    integer:: n, i 
    factorial = 1.0

    if(n/=0) then
        do i=1,n
            factorial = factorial*real(i)
        end do    
    end if

end