program Ejercicio1_Errores
   
    implicit none

    ! Para minimizar los errores de cancelación del ejercicio,
    ! se utiliza el atributo kind de un tipo de dato Real
    ! Este garantiza por lo menos 15 cifras significativas
    integer, parameter :: dp = selected_real_kind(15, 307)
    real(kind=dp):: a=1, b=-2, c=0.01, r1, r2, d

    ! Se calcula la primera raiz, donde el único error de cancelación
    ! apreciable se encuentra en el cálculo del discriminante
    d = sqrt(b**2-4*a*c)

    ! Como b es menor a cero, el cálculo de la raiz r1 no contempla
    ! errores de cancelación
    r1 = (-b + d)/(2*a)

    ! De calcular r2 con la expresión (-b-d)/(2*a) se incurre
    ! en un error de cancelación en el numerador, por ello
    ! se calcula r2 mediante la expresión c/(a*r1), determinada algebraicamente
    r2 = c/(a*r1)

    print*, "El valor de la primera raiz es: ", r1 ! 1.9949874372189416
    print*, "El valor de la segunda raiz es: ", r2 ! 5.0125627810583169E-003
  
end program Ejercicio1_Errores