module myFunctions
    implicit none
    contains

    function func(x) result(y)
        real, intent(in):: x
        real:: y
        y = exp(x) - 2.0
    end function func
    
end module myFunctions

program bisectionMethod
    use myFunctions, only: func
    implicit none

    ! DECLARING VARIABLES 
    real:: a,b,c,fa,fb,fc,e,t=0.01
    real, dimension(:,:), allocatable:: dataset
    real, dimension(:,:), allocatable:: matrix
    integer:: i,j,k

    ! INPUT DATA
    a = 0.0; c = 2.0
    allocate(dataset(0:999,0:7))

    ! OPERATIONS
    do i=0,999
        b = (c+a)/2.0; fa = func(a); fb = func(b); fc = func(c)
        e = (c-a)/2.0
        dataset(i,:) = (/real(i),a,b,c,fa,fb,fc,e/)
        if(fb>0.0) c = b; if (fb<0.0) a = b    
        if(e<=t) exit
    end do 

    ! RESULTS
    allocate(matrix(0:i,0:7))

    do j=0,i
        matrix(j,:) = dataset(j,:)
    end do 

    deallocate(dataset)

    print*, "La raiz está en x = ", b

    write(*,"(a2, 1x, a7, 1x, 5(a8, 1x), a10)") "i", "a", "b", "c", "f(a)", "f(b)", "f(c)", "error"
    do j=0,i
        write(*,"(f3.1, 1x, 7(f8.4, 1x))") (matrix(j,k), k=0,7)
    end do 

end program bisectionMethod

