PROGRAM main
IMPLICIT NONE
 
DOUBLE PRECISION :: PI=3.141592653589793238462643383279502884197
DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:,:) :: Ku, XYZ, MAG
DOUBLE PRECISION :: vec_h(3),MAG_aux(3),Hext_stg(3,3)
DOUBLE PRECISION :: dThet,hext,dh,Ej,Ei,nrj,phi,t,thet,func,mag_total
INTEGER :: N, MCs, iN, i, j, k,nhext
 
  N = 1000    ! number of nanoparticles
 
  MCs = 200   ! Monte Carlo steps
  dTHET = 30.0*PI/180.0
 
 ALLOCATE(Ku(N,3),XYZ(N,3),MAG(N,3))
 
 CALL SRAND(TIME())
 
 DO i=1,N
     thet=RAND()*PI     ! polar angle (theta)
     phi=RAND()*2.*PI     ! azimutal angle (phi)

    ! initial random positions of nanoparticles

     XYZ(i,1)=sin(thet)*cos(phi)
     XYZ(i,2)=sin(thet)*sin(phi)
     XYZ(i,3)=cos(thet)
     
     thet=RAND()*PI     ! polar angle (theta)
     phi=RAND()*2.*PI     ! azimutal angle (phi)
     
     ! initial random directions of nanoparticles
     MAG(i,1) = sin(thet)*cos(phi)
     MAG(i,2) = sin(thet)*sin(phi)
     MAG(i,3) = cos(thet)
     
     thet=RAND()*PI     ! polar angle (theta)
     phi=RAND()*2.*PI     ! azimutal angle (phi)
     
     ! uniaxial directions of nanoparticles
     Ku(i,1)=sin(thet)*cos(phi)
     Ku(i,2)=sin(thet)*sin(phi)
     ku(i,3)=cos(thet)
 END DO
 
 OPEN(UNIT=20,FILE='hex_mag.dat',ACTION='write',STATUS='REPLACE')
 
 !! exp(-e/t)    ---- e = E/2KV and t = kBT/2KV
 t = 0.1
 vec_h(:)=(/1.0,0.0,0.0/)   ! field direction
 
 Hext_stg(1,:) = (/0.0,  1.0, 60./)  !  initial, final, steps number
 Hext_stg(2,:) = (/1.0, -1.0, 100./)  ! initial, final, steps number
 Hext_stg(3,:) = (/-1.0,  1.0, 100./)  ! initial, final, steps number
 
 dh = (1.0 - 0.0)/100
 
 mag_total=0.0
 DO j=1,N
     mag_total = mag_total + dot_product(MAG(j,:),vec_h)
 END DO
 write(20,*) hext,mag_total
 
 nhext=INT(Hext_stg(1,3)+Hext_stg(2,3)+Hext_stg(3,3)-3)
 DO i=1,nhext
    if(i<=Hext_stg(1,3)-1)then
       dh = (Hext_stg(1,2)-Hext_stg(1,1))/(Hext_stg(1,3)-1)
    else if (i<=Hext_stg(1,3)+Hext_stg(2,3)-1)then
       dh = (Hext_stg(2,2)-Hext_stg(2,1))/(Hext_stg(2,3)-1)
    else
       dh = (Hext_stg(3,2)-Hext_stg(3,1))/(Hext_stg(3,3)-1)
    end if
    hext = hext + dh
    
     DO j=1,MCs
         DO k=1,N
             ! we choose randomly a nanoparticle
             iN = NINT(RAND()*N)
             
             Ei = func(Ku(iN,:),MAG(iN,:),vec_h,hext)
             MAG_aux(:)=MAG(iN,:)
             
             CALL genedTHET(MAG_aux,dTHET)
             Ej = func(Ku(iN,:),MAG_aux(:),vec_h,hext)
             
             if(Ej<=Ei)then
                 MAG(iN,:) = MAG_aux(:)
             ELSE
                 nrj = RAND()
                 IF(nrj<exp(-(Ej-Ei)/t))THEN
                      MAG(iN,:) = MAG_aux
                 END IF
             END IF
         END DO
     END DO

     mag_total=0.0
     DO j=1,N
        mag_total = mag_total + dot_product(MAG(j,:),vec_h)
     END DO
     
     write(20,*) hext,mag_total
     print*, i,hext,mag_total

 END DO
 close(20)
 print*, 'final'
END PROGRAM

FUNCTION func(Ku,MAG,vec_h,hext)
    IMPLICIT NONE
    
    DOUBLE PRECISION, DIMENSION(3), INTENT(IN):: Ku,MAG,vec_h
    DOUBLE PRECISION, INTENT(IN) :: hext
    DOUBLE PRECISION :: func
    
    !!  hext = 2H/HA = Ms.H/K
    !!  func = E/KV
    !!  E = -KV cos^2(thet) -MsVHcos(thet)
    
    func = -0.5*(dot_product(Ku,MAG))**2-hext*dot_product(MAG,vec_h)
    
END FUNCTION

SUBROUTINE genedTHET(MAG,dTHET)
    IMPLICIT NONE
    
    DOUBLE PRECISION,PARAMETER :: PI=3.141592653589793238462643383279502884197
    DOUBLE PRECISION, DIMENSION(3), INTENT(INOUT) :: MAG
    DOUBLE PRECISION, INTENT(IN) :: dTHET
    DOUBLE PRECISION :: phi, thet, A(3), B(3),mx,my,mz, newMAG(3),Aux(3)
    
    phi = RAND()*2.0*PI
    thet = RAND()*dTHET
    
    !! building the vector A
    
    mx = MAG(1); my=MAG(2); mz=MAG(3)
    IF(mx==0.0)THEN
        A(1) = 0.0; A(2) = -mz/sqrt(mz**2+my**2); A(3) = my/sqrt(mz**2+my**2)
    ELSE IF(my==0.0)THEN
        A(1) = -mz/sqrt(mz**2+mx**2); A(2) = 0.0; A(3) = mx/sqrt(mz**2+mx**2)
    ELSE
        A(1) = -mz*mx/(sqrt(mx**2+my**2))
        A(2) = -mz*my/(sqrt(mx**2+my**2))
        A(3) = sqrt(mx**2+my**2)
    END IF
    
    !! building the vector B
    B(1) =  A(2)*mz-A(3)*my
    B(2) = -A(1)*mz+A(3)*mx
    B(3) =  A(1)*my-A(2)*mx
    
    Aux=sin(thet)*cos(phi)*A + sin(thet)*sin(phi)*B
    newMAG = cos(thet)*MAG+Aux
    
    MAG = newMAG
    
    END SUBROUTINE