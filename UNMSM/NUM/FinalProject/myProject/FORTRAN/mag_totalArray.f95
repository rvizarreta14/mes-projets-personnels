
subroutine mag_totalArray(mag_total, mag, vec_h, n)

    integer, parameter :: dp = selected_real_kind(15, 307)

    ! VARIABLE DECLARATION

    real(kind=dp), intent(in):: mag(n,3), vec_h(3)
    integer, intent(in):: n
    real(kind=dp), intent(inout):: mag_total
    integer:: j

    ! FILLING mag_total

    do j=1,n 
        mag_total = mag_total + dot_product(mag(j,:), vec_h)
    end do 

end subroutine
