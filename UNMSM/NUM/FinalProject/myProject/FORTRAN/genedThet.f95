
subroutine genedThet(mag_aux, dThet, pi)

    integer, parameter :: dp = selected_real_kind(15, 307)

    ! VARIABLE DECLARATION

    real(kind=dp), intent(in):: dThet, pi
    real(kind=dp), intent(inout):: mag_aux(3)
    real(kind=dp):: phi, thet, a(3), b(3), mx, my, mz

    ! SETTING phi AND thet

    phi = 2.0*pi*rand()
    thet = dThet*rand()

    ! BUILDING VECTOR A

    mx = mag_aux(1) ; my = mag_aux(2) ; mz = mag_aux(3)

    if (mx==0.0) then
        A(1) = 0.0
        A(2) = -mz/sqrt(mz**2+my**2)
        A(3) = my/sqrt(mz**2+my**2)
    
    else if (my == 0.0) then 
        A(1) = -mz/sqrt(mz**2+mx**2)
        A(2) = 0.0
        A(3) = mx/sqrt(mz**2+mx**2)

    else 
        A(1) = -mz*mx/sqrt(my**2+mx**2)
        A(2) = -mz*my/sqrt(mx**2+my**2)
        A(3) = sqrt(mx**2+my**2)
    
    end if

    ! BUILDING VECTOR B

    B(1) = A(2)*mz - A(3)*my
    B(2) = -A(1)*mz + A(3)*mx 
    B(3) = A(1)*my - A(2)*mx 

    mag_aux = cos(thet)*mag_aux + sin(thet)*cos(phi)*A + sin(thet)*sin(phi)*B 

end subroutine