module myModule
    implicit none 

    contains 
        include "mag_totalArray.f95"
        include "randomArrays.f95"
        include "abc.f95"
        include "genedThet.f95"
        include "hextCalc.f95"
end module


program main
    use myModule
    implicit none

    ! DECLARING VARIABLES

    ! Using kind attribute as a real kind of data for the double precision
    ! Doing this we assure at least 15 significant numbers
    integer, parameter :: dp = selected_real_kind(15, 307)
    ! Defining pi as a constant
    real(kind=dp), parameter:: pi=3.141592653589793238462643383279502884197
    ! Defining arrays
    real(kind=dp), allocatable, dimension(:,:):: ku, mag
    ! Defining vectors
    real(kind=dp):: vec_h(3), mag_aux(3), hext_stg(3,3) 
    ! Defining aditional variables and function hextCalc
    real(kind=dp):: dThet, hext, dh, mag_total
    !real:: hextCalc
    integer:: n, mcs, i, j, k, nhext

    ! READING INPUT DATA

    ! print*, "Write the number of nanoparticles"
    ! read*, n 
    ! print*, "Write the number of Monte Carlo steps"
    ! read*, mcs
    ! print*, "Write the angle dThet in degrees"
    ! read*, dThet
    n = 1000
    mcs = 20
    dThet = 10
    dThet = dThet*pi/180.0 ! Converting degrees to radians

    ! ALLOCATING SPACE IN MEMORY FOR ARRAYS

    allocate(ku(n,3), mag(n,3))

    ! FILLING ARRAYS ku, mag

    call randomArrays(pi, n, mag, ku)

    ! OPENING FILE hex_mag

    open(unit=1, file="hex_mag", status='replace', action='write')

    ! INITIALIZING VARIABLES

    vec_h(:) = (/1.0, 0.0,0.0/) ! Field direction
    hext_stg(1,:) = (/0.0, 1.0, 60./) ! Initial, final, steps number
    hext_stg(2,:) = (/1.0, -1.0, 100./) ! Initial, final, steps number
    hext_stg(3,:) = (/-1.0, 1.0, 100./) ! Initial, final, steps number
    dh = (1.0-0.0)/100
    mag_total = 0.0

    ! SETTING mag_total AND FILLING FILE hex_mag

    call mag_totalArray(mag_total, mag, vec_h, n)
    write(1,*) hext, mag_total

    ! INITIALIZING VARIABLE nhext

    nhext = int(hext_stg(1,3) + hext_stg(2,3) + hext_stg(3,3) - 3)

    ! SETTING mag_total AND FILE hex_mag USING MARKOV CHAINS

    do i=1,nhext

        hext = hextCalc(hext, dh, hext_stg, i)
        
        do j=1, mcs
            do k=1, n 
                call abc(n, mag, vec_h, hext, dThet, ku, mag_aux, pi)
            end do 
        end do 

        mag_total = 0.0

        do j=1,n 
            mag_total = mag_total + dot_product(mag(j,:), vec_h)
        end do 

        write(1,*) hext, mag_total
        print*, hext, mag_total         
        
    end do 

end program main