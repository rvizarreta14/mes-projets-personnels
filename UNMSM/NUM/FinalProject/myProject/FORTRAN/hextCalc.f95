
real function hextCalc(hext, dh, hext_stg, i)

    integer, parameter :: dp = selected_real_kind(15, 307)

    ! VARIABLE DECLARATION

    real(kind=dp):: dh, hext 
    real(kind=dp):: hext_stg(3,3) 
    integer:: i

    ! MIDIFYING dh

    if (i <= hext_stg(1,3)-1) then
        dh = (hext_stg(1,2)-hext_stg(1,1))/(hext_stg(1,3)-1)

        else if (i <= hext_stg(1,3)+hext_stg(2,3)-1) then
            dh = (hext_stg(2,2)-hext_stg(2,1))/(hext_stg(2,3)-1) 
        else 
            dh = (hext_stg(3,2)-hext_stg(3,1))/(hext_stg(3,3)-1)

    end if

    ! SETTING hext

    hextCalc = hext + dh

end