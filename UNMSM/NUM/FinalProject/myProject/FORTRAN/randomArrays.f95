
subroutine randomArrays(pi, n, mag, ku)

    integer, parameter :: dp = selected_real_kind(15, 307)

    ! VARIABLE DECLARATION
    
    real(kind=dp), intent(in):: pi
    integer, intent(in):: n
    real(kind=dp), intent(out):: mag(n,3), ku(n,3)
    real(kind=dp):: thet, phi
    integer:: i 

    ! GENERATE A SET OF ALEATORY NUMBERS

    call srand(time())

    ! FILLING ARRAYS

    do i=1,n

        thet = pi*rand() ! Polar angle theta
        phi = 2.0*pi*rand() !Azimutal angle phi

        ! Initial random directions of nanoparticles
        mag(i,1) = sin(thet)*cos(phi)
        mag(i,2) = sin(thet)*sin(phi)
        mag(i,3) = cos(thet)

        thet = pi*rand() ! Polar angle theta
        phi = 2.0*pi*rand() !Azimutal angle phi

        ! Uniaxial random directions of nanoparticles
        ku(i,1) = sin(thet)*cos(phi)
        ku(i,2) = sin(thet)*sin(phi)
        ku(i,3) = cos(thet)

    end do 

end subroutine