subroutine abc(n, mag, vec_h, hext, dThet, ku, mag_aux, pi)

    integer, parameter :: dp = selected_real_kind(15, 307)

    ! VARIABLE DECLARATION

    real(kind=dp), intent(in):: vec_h(3), hext, ku(n,3), dThet, pi
    integer, intent(in):: n
    real(kind=dp), intent(inout):: mag(n,3), mag_aux(3)
    real(kind=dp):: ei, ej, nrj, t
    integer:: in 

    ! SETTING mag VALUES

    in = nint(n*rand()) ! Random nanoparticle
    ei = -0.5*(dot_product(ku(iN,:),mag(iN,:)))**2 - hext*dot_product(mag(iN,:),vec_h)
    mag_aux(:) = mag(in,:)
    call genedThet(mag_aux, dThet, pi) 
    ej = -0.5*(dot_product(ku(iN,:),mag_aux(:)))**2 - hext*dot_product(mag_aux(:),vec_h)
    t = 0.1

    if (ej <= ei) then
        mag(in,:) = mag_aux(:)
    else 
        nrj = rand()
        if (nrj < exp(-(ej-ei)/t)) then 
            mag(in,:) = mag_aux 
        end if   
    end if


end subroutine