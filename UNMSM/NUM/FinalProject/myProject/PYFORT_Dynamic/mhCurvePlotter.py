#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import matplotlib as mpl


# In[10]:


def animate(i):
    
    while True:        
        try:
            
            with open("mhData.txt") as file:
                mh = np.asarray([[float(num) for num in line.split('\t')] for line in file])
            xar=mh[:,0]
            yar=mh[:,1]

            ax.clear()
            ax.plot(xar,yar,'.', markersize=16, color="#70191C")
            ax.set_title('Curva M(H)', size="xx-large", family="serif", weight= "semibold") # https://matplotlib.org/3.1.1/gallery/text_labels_and_annotations/fonts_demo.html ==== PERSONALIZE FONTS
            plt.xlabel('Campo aplicado H[T]', fontsize= 20, color="#000000", family="serif") # https://matplotlib.org/3.1.0/gallery/color/named_colors.html ==== LIST OF COLORS
            plt.ylabel('Magnetización [M]', fontsize= 20, color="#000000", family="serif")
            plt.rcParams.update({'font.size': 12})                      
            plt.xlim(-1.05, 1)
            plt.ylim(-1050, 1000)
            ax.set_facecolor('#FFFFFF')
            plt.gcf().set_facecolor('#FFFBFB')
            ax.tick_params(labelsize=15)
            break
        except (ValueError,IndexError):
            print("Error!!!!")


# In[11]:


fig = plt.figure(figsize=(15,15))
plt.style.use("bmh")
ax = fig.add_subplot(1,1,1)
ani = animation.FuncAnimation(fig,animate,interval=10)
plt.show()


# In[ ]:





# In[ ]:




