module fortran
    implicit none
    contains

    ! --------------------------------------------------------------
    ! SETTING MAG AND MAGAUX
    subroutine subr1(vecH, hext, dThet, ku, pi, magIn, mag)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        real(kind=dp), intent(in):: vecH(3), hext, dThet, pi, ku(3), magIn(3)
        real(kind=dp), intent(out):: mag(3)
        real(kind=dp):: ei, ej, nrj, t, magAux(3)
        ! SETTING mag VALUES
        mag = magIn
        ei = -0.5*(dot_product(ku,mag))**2 - hext*dot_product(mag,vecH)
        magAux(:) = mag
        call genedThet(magAux, dThet, pi) 
        ej = -0.5*(dot_product(ku,magAux(:)))**2 - hext*dot_product(magAux(:),vecH)
        t = 0.1
        if (ej <= ei) then
            mag = magAux(:)
        else 
            nrj = rand()
            if (nrj < exp(-(ej-ei)/t)) then 
                mag = magAux 
            end if   
        end if

    end subroutine

    ! --------------------------------------------------------------
    ! SETTING MAGAUX
    subroutine genedThet(mag_aux, dThet, pi)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        real(kind=dp), intent(in):: dThet, pi
        real(kind=dp), intent(inout):: mag_aux(3)
        real(kind=dp):: phi, thet, a(3), b(3), mx, my, mz
        ! SETTING phi AND thet
        phi = 2.0*pi*rand()
        thet = dThet*rand()
        ! BUILDING VECTOR A
        mx = mag_aux(1) ; my = mag_aux(2) ; mz = mag_aux(3)
        if (mx==0.0) then
            A(1) = 0.0
            A(2) = -mz/sqrt(mz**2+my**2)
            A(3) = my/sqrt(mz**2+my**2)        
        else if (my == 0.0) then 
            A(1) = -mz/sqrt(mz**2+mx**2)
            A(2) = 0.0
            A(3) = mx/sqrt(mz**2+mx**2)
        else 
            A(1) = -mz*mx/sqrt(my**2+mx**2)
            A(2) = -mz*my/sqrt(mx**2+my**2)
            A(3) = sqrt(mx**2+my**2)        
        end if
        ! BUILDING VECTOR B
        B(1) = A(2)*mz - A(3)*my
        B(2) = -A(1)*mz + A(3)*mx 
        B(3) = A(1)*my - A(2)*mx 
        mag_aux = cos(thet)*mag_aux + sin(thet)*cos(phi)*A + sin(thet)*sin(phi)*B 

    end subroutine

    ! --------------------------------------------------------------
    ! CALCULATING HEX
    subroutine subr2(hext, dh_t, hext_stg, i, hext2)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        real(kind=dp), intent(in):: dh_t, hext_stg(3,3), hext 
        integer, intent(in):: i
        real(kind=dp), intent(out):: hext2
        real(kind=dp):: dh
        ! MODIFYING dh
        dh = dh_t
        if (i <= hext_stg(1,3)-1) then
            dh = (hext_stg(1,2)-hext_stg(1,1))/(hext_stg(1,3)-1)
            else if (i <= hext_stg(1,3)+hext_stg(2,3)-1) then
                dh = (hext_stg(2,2)-hext_stg(2,1))/(hext_stg(2,3)-1) 
            else 
                dh = (hext_stg(3,2)-hext_stg(3,1))/(hext_stg(3,3)-1)
        end if
        ! SETTING hext
        hext2 = hext + dh

    end subroutine

    ! --------------------------------------------------------------
    ! FILLING ARRAYS 
    subroutine subr3(pi, n, mag, ku, xyz)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION        
        real(kind=dp), intent(in):: pi
        integer, intent(in):: n
        real(kind=dp), intent(out):: mag(n,3), ku(n,3), xyz(n,3)
        real(kind=dp):: thet, phi
        integer:: i 
        ! GENERATE A SET OF ALEATORY NUMBERS
        call srand(time())
        ! FILLING ARRAYS
        do i=1,n
            
            thet=RAND()*PI     ! polar angle (theta)
            phi=RAND()*2.*PI     ! azimutal angle (phi)
       
           ! initial random positions of nanoparticles
       
            XYZ(i,1)=sin(thet)*cos(phi)
            XYZ(i,2)=sin(thet)*sin(phi)
            XYZ(i,3)=cos(thet)

            ! XYZ(i,1)=RAND()
            ! XYZ(i,2)=RAND()
            ! XYZ(i,3)=RAND()

            thet = pi*rand() ! Polar angle theta
            phi = 2.0*pi*rand() !Azimutal angle phi

            ! Initial random directions of nanoparticles
            mag(i,1) = sin(thet)*cos(phi)
            mag(i,2) = sin(thet)*sin(phi)
            mag(i,3) = cos(thet)

            thet = pi*rand() ! Polar angle theta
            phi = 2.0*pi*rand() !Azimutal angle phi

            ! Uniaxial random directions of nanoparticles
            ku(i,1) = sin(thet)*cos(phi)
            ku(i,2) = sin(thet)*sin(phi)
            ku(i,3) = cos(thet)
        end do 

    end subroutine

    ! --------------------------------------------------------------
    ! CALCULATING TOTAL MAGNETIC FIELD
    subroutine subr4(mag, vec_h, n,mag_total)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        real(kind=dp), intent(in):: mag(n,3), vec_h(3)
        integer, intent(in):: n
        real(kind=dp), intent(out):: mag_total 
        integer:: j
        ! FILLING mag_total
        mag_total = 0
        do j=1,n 
            mag_total = mag_total + dot_product(mag(j,:), vec_h)
        end do 

    end subroutine

    ! --------------------------------------------------------------

    ! MARKOV CHAIN
    subroutine subr5(mcs,n,mag,hext,vecH,dThet,ku,pi,magTotal)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        integer, intent(in):: mcs, n
        real(kind=dp), intent(in):: mag(n,3), vecH(3), ku(n,3), hext, dThet, pi
        real(kind=dp), intent(out):: magTotal
        integer:: j,k,randN
        real(kind=dp):: magT(n,3)

        ! DETERMINATING magTotal
        magT = mag
        do j=1, mcs
            do k=1, n 
                randN = nint(n*rand()) ! Random nanoparticle
                call subr6(vecH, hext, dThet, ku(randN,:), pi, magT(randN,:))
            end do 
        end do        
        call subr4(mag,vecH,n,magTotal)

    end subroutine

        ! --------------------------------------------------------------
    ! SETTING MAG AND MAGAUX 2
    subroutine subr6(vecH, hext, dThet, ku, pi, mag)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        real(kind=dp), intent(in):: vecH(3), hext, dThet, pi, ku(3)
        real(kind=dp), intent(inout):: mag(3)
        real(kind=dp):: ei, ej, nrj, t, magAux(3)
        ! SETTING mag VALUES
        ei = -0.5*(dot_product(ku,mag))**2 - hext*dot_product(mag,vecH)
        magAux(:) = mag
        call genedThet(magAux, dThet, pi) 
        ej = -0.5*(dot_product(ku,magAux(:)))**2 - hext*dot_product(magAux(:),vecH)
        t = 0.1
        if (ej <= ei) then
            mag = magAux(:)
        else 
            nrj = rand()
            if (nrj < exp(-(ej-ei)/t)) then 
                mag = magAux 
            end if   
        end if

    end subroutine

end module fortran