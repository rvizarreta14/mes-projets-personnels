#!/usr/bin/env python
# coding: utf-8

# In[1]:


from IPython.display import HTML
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D


# In[2]:


# OPEN AND READ FILE xyaData.txt
with open("xyzData.txt") as file:
    xyz = np.asarray([[float(num) for num in line.split('\t')] for line in file])
dfXyz = pd.DataFrame(data=xyz[0:,0:],columns=['x','y','z']) 


# In[3]:


def animate(i):
    
    while True:        
        try:
            with open("magData.txt") as file:
                mag = np.asarray([[float(num) for num in line.split('\t')] for line in file])
            dfMag = pd.DataFrame(data=mag[0:,0:],columns=['x','y','z']) # CREATE DATAFRAME FOR ARRAY
            ax.clear()    
            ax.quiver(dfXyz.x, dfXyz.y, dfXyz.z,dfMag.x, dfMag.y, dfMag.z, length=0.1, color='g')
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_zlabel('z')
            fig.canvas.draw()
            break
        except (ValueError,IndexError):
            print("Error!!!!")


# In[4]:


fig = plt.figure(figsize=(15,15))
ax = fig.add_subplot(111, projection='3d')
ani = animation.FuncAnimation(fig,animate,interval=100)
plt.show()


# In[ ]:




