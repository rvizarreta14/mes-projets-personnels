#!/usr/bin/env python
# coding: utf-8

# In[13]:


import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import matplotlib as mpl


# In[68]:


def animate(i):
    
    while True:        
        try:
            
            with open("mhData.txt") as file:
                mh = np.asarray([[float(num) for num in line.split('\t')] for line in file])
            xar=mh[:,0]
            yar=mh[:,1]

            ax.clear()
            ax.plot(xar,yar,'.', markersize=16, color="#19198C")
            ax.set_title('M(H) Curve', size="xx-large", family="serif", weight= "semibold")
            plt.xlabel('H(T)', fontsize= 20, color="#000000", family="serif")
            plt.ylabel('M', fontsize= 20, color="#000000", family="serif")
            plt.rcParams.update({'font.size': 12})                      
            plt.xlim(-1.05, 1)
            plt.ylim(-1050, 1000)
            break
        except (ValueError,IndexError):
            print("Error!!!!")


# In[69]:


fig = plt.figure(figsize=(15,15))
plt.style.use("bmh")
ax = fig.add_subplot(1,1,1)
ani = animation.FuncAnimation(fig,animate,interval=10)
plt.show()


# In[ ]:





# In[ ]:




