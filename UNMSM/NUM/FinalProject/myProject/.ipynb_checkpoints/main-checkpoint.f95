program main
    implicit none

    ! DECLARING VARIABLES

    ! Using kind attribute as a real kind of data for the double precision
    ! Doing this we assure at least 15 significant numbers
    integer, parameter :: dp = selected_real_kind(15, 307)
    ! Defining pi as a constant
    real(kind=dp), parameter:: pi=3.141592653589793238462643383279502884197
    ! Defining arrays
    real(kind=dp), allocatable, dimension(:,:):: ku, mag
    ! Defining vectors
    real(kind=dp):: vec_h(3), mag_aux(3), hext_stg(3,3) 
    ! Defining aditional variables and function hextCalc
    real(kind=dp):: dThet, hext, dh, mag_total
    real:: hextCalc
    integer:: n, mcs, i, j, k, nhext

    ! READING INPUT DATA

    print*, "Write the number of nanoparticles"
    read*, n 
    print*, "Write the number of Monte Carlo steps"
    read*, mcs
    print*, "Write the angle dThet in degrees"
    read*, dThet
    dThet = dThet*pi/180.0 ! Converting degrees to radians

    ! ALLOCATING SPACE IN MEMORY FOR ARRAYS

    allocate(ku(n,3), mag(n,3))

    ! FILLING ARRAYS ku, mag

    call randomArrays(pi, n, mag, ku)

    ! OPENING FILE hex_mag

    open(unit=1, file="hex_mag", status='replace', action='write')

    ! INITIALIZING VARIABLES

    vec_h(:) = (/1.0, 0.0,0.0/) ! Field direction
    hext_stg(1,:) = (/0.0, 1.0, 60./) ! Initial, final, steps number
    hext_stg(2,:) = (/1.0, -1.0, 100./) ! Initial, final, steps number
    hext_stg(3,:) = (/-1.0, 1.0, 100./) ! Initial, final, steps number
    dh = (1.0-0.0)/100
    mag_total = 0.0

    ! FILLING ARRAY mag_total AND FILE hex_mag

    call mag_totalArray(mag_total, mag, vec_h, n)
    write(1,*) hext, mag_total

    ! INITIALIZING VARIABLE nhext

    nhext = int(hext_stg(1,3) + hext_stg(2,3) + hext_stg(3,3) - 3)

    ! FILLING ARRAY mag_total AND FILE hex_mag USING MARKOV CHAINS

    do i=1,nhext

        hext = hextCalc(hext, dh, hext_stg, i)
        
        do j=1, mcs
            do k=1, n 
                call abc(n, mag, vec_h, hext, dThet, ku, mag_aux, pi)
            end do 
        end do 

        mag_total = 0.0

        do j=1,n 
            mag_total = mag_total + dot_product(mag(j,:), vec_h)
        end do 

        write(1,*) hext, mag_total
        print*, hext, mag_total         

    end do 



end program main

subroutine abc(n, mag, vec_h, hext, dThet, ku, mag_aux, pi)

    integer, parameter :: dp = selected_real_kind(15, 307)

    ! VARIABLE DECLARATION

    real(kind=dp), intent(in):: vec_h(3), hext, ku(n,3), dThet, pi
    integer, intent(in):: n
    real(kind=dp), intent(inout):: mag(n,3), mag_aux(3)
    real(kind=dp):: ei, ej, nrj, t
    integer:: in 

    ! SETTING mag VALUES

    in = nint(n*rand()) ! Random nanoparticle
    ei = -0.5*(dot_product(ku(iN,:),mag(iN,:)))**2 - hext*dot_product(mag(iN,:),vec_h)
    mag_aux(:) = mag(in,:)
    call genedThet(mag_aux, dThet, pi) 
    ej = -0.5*(dot_product(ku(iN,:),mag_aux(:)))**2 - hext*dot_product(mag_aux(:),vec_h)

    if (ej <= ei) then
        mag(in,:) = mag_aux(:)
    else 
        nrj = rand()
    end if

    t = 0.1

    if (nrj < exp(-(ej-ei)/t)) then 
        mag(in,:) = mag_aux 
    end if   

end subroutine


subroutine genedThet(mag_aux, dThet, pi)

    integer, parameter :: dp = selected_real_kind(15, 307)

    ! VARIABLE DECLARATION

    real(kind=dp), intent(in):: dThet, pi
    real(kind=dp), intent(inout):: mag_aux(3)
    real(kind=dp):: phi, thet, a(3), b(3), mx, my, mz

    ! SETTING phi AND thet

    phi = 2.0*pi*rand()
    thet = dThet*rand()

    ! BUILDING VECTOR A

    mx = mag_aux(1) ; my = mag_aux(2) ; mz = mag_aux(3)

    if (mx==0.0) then
        A(1) = 0.0
        A(2) = -mz/sqrt(mz**2+my**2)
        A(3) = my/sqrt(mz**2+my**2)
    
    else if (my == 0.0) then 
        A(1) = -mz/sqrt(mz**2+mx**2)
        A(2) = 0.0
        A(3) = mx/sqrt(mz**2+mx**2)

    else 
        A(1) = -mz*mx/sqrt(my**2+mx**2)
        A(2) = -mz*my/sqrt(mx**2+my**2)
        A(3) = sqrt(mx**2+my**2)
    
    end if

    ! BUILDING VECTOR B

    B(1) = A(2)*mz - A(3)*my
    B(2) = -A(1)*mz + A(3)*mx 
    B(3) = A(1)*my - A(2)*mx 

    mag_aux = cos(thet)*mag_aux + sin(thet)*cos(phi)*A + sin(thet)*sin(phi)*B 

end subroutine


real function hextCalc(hext, dh, hext_stg, i)

    integer, parameter :: dp = selected_real_kind(15, 307)

    ! VARIABLE DECLARATION

    real(kind=dp):: dh, hext 
    real(kind=dp):: hext_stg(3,3) 
    integer:: i

    ! MIDIFYING dh

    if (i <= hext_stg(1,3)-1) then
        dh = (hext_stg(1,2)-hext_stg(1,1))/(hext_stg(1,3)-1)

        else if (i <= hext_stg(1,3)+hext_stg(2,3)-1) then
            dh = (hext_stg(2,2)-hext_stg(2,1))/(hext_stg(2,3)-1) 
        else 
            dh = (hext_stg(3,2)-hext_stg(3,1))/(hext_stg(3,3)-1)

    end if

    ! SETTING hext

    hextCalc = hext + dh

end


subroutine mag_totalArray(mag_total, mag, vec_h, n)

    integer, parameter :: dp = selected_real_kind(15, 307)

    ! VARIABLE DECLARATION

    real(kind=dp), intent(in):: mag(n,3), vec_h(3)
    integer, intent(in):: n
    real(kind=dp), intent(inout):: mag_total
    integer:: j

    ! FILLING mag_total

    do j=1,n 
        mag_total = mag_total + dot_product(mag(j,:), vec_h)
    end do 

end subroutine



subroutine randomArrays(pi, n, mag, ku)

    integer, parameter :: dp = selected_real_kind(15, 307)

    ! VARIABLE DECLARATION
    
    real(kind=dp), intent(in):: pi
    integer, intent(in):: n
    real(kind=dp), intent(out):: mag(n,3), ku(n,3)
    real(kind=dp):: thet, phi
    integer:: i 

    ! GENERATE A SET OF ALEATORY NUMBERS

    call srand(time())

    ! FILLING ARRAYS

    do i=1,n

        thet = pi*rand() ! Polar angle theta
        phi = 2.0*pi*rand() !Azimutal angle phi

        ! Initial random directions of nanoparticles
        mag(i,1) = sin(thet)*cos(phi)
        mag(i,2) = sin(thet)*sin(phi)
        mag(i,3) = cos(thet)

        thet = pi*rand() ! Polar angle theta
        phi = 2.0*pi*rand() !Azimutal angle phi

        ! Uniaxial random directions of nanoparticles
        ku(i,1) = sin(thet)*cos(phi)
        ku(i,2) = sin(thet)*sin(phi)
        ku(i,3) = cos(thet)

    end do 

    end subroutine