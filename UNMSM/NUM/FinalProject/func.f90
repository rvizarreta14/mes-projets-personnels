FUNCTION func(Ku,MAG,vec_h,hext)
IMPLICIT NONE

DOUBLE PRECISION, DIMENSION(3), INTENT(IN):: Ku,MAG,vec_h
DOUBLE PRECISION, INTENT(IN) :: hext
DOUBLE PRECISION :: func

!!  hext = 2H/HA = Ms.H/K
!!  func = E/KV
!!  E = -KV cos^2(thet) -MsVHcos(thet)

func = -0.5*(dot_product(Ku,MAG))**2-hext*dot_product(MAG,vec_h)

END FUNCTION
