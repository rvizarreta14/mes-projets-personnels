SUBROUTINE genedTHET(MAG,dTHET)
IMPLICIT NONE

DOUBLE PRECISION,PARAMETER :: PI=3.141592653589793238462643383279502884197
DOUBLE PRECISION, DIMENSION(3), INTENT(INOUT) :: MAG
DOUBLE PRECISION, INTENT(IN) :: dTHET
DOUBLE PRECISION :: phi, thet, A(3), B(3),mx,my,mz, newMAG(3),Aux(3)

phi = RAND()*2.0*PI
thet = RAND()*dTHET

!! building the vector A

mx = MAG(1); my=MAG(2); mz=MAG(3)
IF(mx==0.0)THEN
    A(1) = 0.0; A(2) = -mz/sqrt(mz**2+my**2); A(3) = my/sqrt(mz**2+my**2)
ELSE IF(my==0.0)THEN
    A(1) = -mz/sqrt(mz**2+mx**2); A(2) = 0.0; A(3) = mx/sqrt(mz**2+mx**2)
ELSE
    A(1) = -mz*mx/(sqrt(mx**2+my**2))
    A(2) = -mz*my/(sqrt(mx**2+my**2))
    A(3) = sqrt(mx**2+my**2)
END IF

!! building the vector B
B(1) =  A(2)*mz-A(3)*my
B(2) = -A(1)*mz+A(3)*mx
B(3) =  A(1)*my-A(2)*mx

Aux=sin(thet)*cos(phi)*A + sin(thet)*sin(phi)*B
newMAG = cos(thet)*MAG+Aux

MAG = newMAG

END SUBROUTINE