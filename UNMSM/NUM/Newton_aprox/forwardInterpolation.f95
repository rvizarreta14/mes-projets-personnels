program forwardNewton
    implicit none
    
    ! VARIABLE DECLARATION
    double precision, allocatable:: dataset(:,:), coefMatrix(:)
    integer:: n, i, j, lines, m !number of items
    double precision:: s, x, h, factorial, mult, sAcum

    ! READING NUMBER OF ROWS IN FILE "data"
    open(unit=2, file="data")
    do
        read(2,*,end=10)
        lines = lines + 1
    end do
    10 close(2)
    n=lines
    m=n
    ! ALLOCATING SPACE IN MEMORY FOR ARRAY dataset
    allocate(dataset(lines,lines+1))

    ! READING FILE "data"
    open(unit=2, file="data")
    do i=1,lines
        read(2,*) dataset(i,1), dataset(i,2)
    end do     
    close(2)

    ! FILLING ARRAY dataset
    do j=3,lines+1        
           do i=1,lines
                if(i<n) then  
                    dataset(i,j) = dataset(i+1,j-1) - dataset(i,j-1)
                end if
            end do
            n = n-1      
    end do

    ! CALCULATING h
    h = dataset(2,1)-dataset(1,1) !0.1
    print*,h
    ! DETERMINATING THE COEFFICIENT MATRIX
    print*, "Ingrese el valor a calcular: "
    read*, x
    !print*, "Ingrese el número de términos de la fórmula de Newton: "
    !read*, m 

    ! CALCULATING DE LOCAL COORDINATE s
    s = (x-dataset(1,1))/h !7

    ! ALLOCATING SPACE IN MEMORY FOR ARRAY coefMatrix
    allocate(coefMatrix(m+1))

    ! FILLING ARRAY coefMatrix
    do i=1,m
        coefMatrix(i) = (dataset(1,1+i)/factorial(i-1))
    end do

    ! CALCULATING THE REQUIRED VALUE
    mult = coefMatrix(1)
    sAcum = s
    do i=1, m
        mult = mult + coefMatrix(i+1)*sAcum
        sAcum = sAcum*(s-i)
    end do 

    print*, mult
 
    ! PRINTING THE ARRAY
    do i=1,lines
        write(*,"(100(f5.2,2x))") (dataset(i,j), j=1,lines-i+2)
    end do 

end program forwardNewton

double precision function factorial(n)

    integer:: n, i 
    factorial = 1.0

    if(n/=0) then
        do i=1,n
            factorial = factorial*real(i)
        end do    
    end if

end