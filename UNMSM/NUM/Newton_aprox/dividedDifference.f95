program dividedDifference
    implicit none
    
    ! VARIABLE DECLARATION
    double precision, allocatable:: dataset(:,:)
    integer:: n, i, j, lines

    ! READING NUMBER OF ROWS IN FILE "data"
    open(unit=2, file="data")
    do
        read(2,*,end=10)
        lines = lines + 1
    end do
    10 close(2)
    n=lines

    ! ALLOCATING SPACE IN MEMORY FOR ARRAY dataset
    allocate(dataset(lines,lines+1))

    ! READING FILE "data"
    open(unit=2, file="data")
    do i=1,lines
        read(2,*) dataset(i,1), dataset(i,2)
    end do     
    close(2)

    ! FILLING ARRAY dataset
    do j=3,lines+1    
            do i=1,lines
                if(i<n) then  
                    dataset(i,j) = (dataset(i+1,j-1) - dataset(i,j-1))/(dataset(lines-n+1+i,1) - dataset(i,1))
                end if
            end do
            n = n-1      
    end do 
  
    ! PRINTING THE ARRAY
    print*, "TABLA DE DIFERENCIAS DIVIDIDAS"
    print*, "-------------------------------"
    do i=1,lines
        write(*,"(100(f14.10,2x))") (dataset(i,j), j=1,lines-i+2)
    end do 

end program dividedDifference