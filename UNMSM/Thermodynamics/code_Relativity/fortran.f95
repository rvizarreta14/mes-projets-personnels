module fortran
    implicit none
    contains

    ! --------------------------------------------------------------
    ! SETTING MAG AND MAGAUX
    subroutine subr1(vecH, Hext, dThet, ku, pi, magIn, mag, d, kuni, Ms, kb, temp)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        real(kind=dp), intent(in):: Ms, kuni, d, vecH(3), Hext, dThet, pi, ku(3), magIn(3), kb, temp
        real(kind=dp), intent(out):: mag(3)
        real(kind=dp):: ei, ej, nrj, magAux(3), vol

        ! SETTING mag VALUES
        mag = magIn
        vol = (pi/6.0)*d**3
        ei = -kuni*vol*(dot_product(ku,mag))**2 - Ms*Hext*vol*dot_product(mag,vecH)
        magAux(:) = mag
        call genedThet(magAux, dThet, pi) 
        ej = -kuni*vol*(dot_product(ku,magAux))**2 - Ms*Hext*vol*dot_product(magAux,vecH)

        if (ej <= ei) then
            mag = magAux(:)
        else 
            nrj = rand()
            if (nrj < exp(-(ej-ei)/(kb*temp))) then 
                mag = magAux 
            end if   
        end if

    end subroutine

    ! --------------------------------------------------------------
    ! SETTING MAGAUX
    subroutine genedThet(mag_aux, dThet, pi)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        real(kind=dp), intent(in):: dThet, pi
        real(kind=dp), intent(inout):: mag_aux(3)
        real(kind=dp):: phi, thet, hat_r(3), hat_t(3), hat_f(3), rx, ry, rz, mag_mod
        ! SETTING phi AND thet
        phi = 2.0*pi*rand()
        thet = dThet*rand()

        rx = mag_aux(1) ; ry = mag_aux(2) ; rz = mag_aux(3)
       
        hat_r(1)=rx
        hat_r(2)=ry
        hat_r(3)=rz

        hat_t(1)=rz*rx/sqrt(rx**2+ry**2)
        hat_t(2)=rz*ry/sqrt(rx**2+ry**2)
        hat_t(3)=-sqrt(rx**2+ry**2)

        hat_f(1)=-ry/sqrt(rx**2+ry**2)
        hat_f(2)=rx/sqrt(rx**2+ry**2)
        hat_f(3)=0.0

        mag_aux(:) = sin(thet)*cos(phi)*hat_t(:) + &
        sin(thet)*sin(phi)*hat_f(:) + &
        cos(thet)*hat_r(:)

        mag_mod = dot_product(mag_aux,mag_aux)
        mag_aux(1) = mag_aux(1)/mag_mod
        mag_Aux(2) = mag_aux(2)/mag_mod 
        mag_Aux(3) = mag_aux(3)/mag_mod

    end subroutine

    ! --------------------------------------------------------------
    ! CALCULATING HEX
    subroutine subr2(hext, nHstep, Hmax, i, hext2)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        real(kind=dp), intent(in):: nHstep, hext, Hmax 
        integer, intent(in):: i
        real(kind=dp), intent(out):: hext2
        real(kind=dp):: dh
        ! MODIFYING dh
        if (i <= nHstep/2) then
            dh = 2*Hmax/nHstep
            else if (i <= 3*nHstep/2-1) then
                dh = -2*Hmax/nHstep 
            else 
                dh = 2*Hmax/nHstep 
        end if
        ! SETTING hext
        hext2 = hext + dh

    end subroutine

    ! --------------------------------------------------------------
    ! FILLING ARRAYS 
    subroutine subr3(pi, n, mag, ku, xyz)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION        
        real(kind=dp), intent(in):: pi
        integer, intent(in):: n
        real(kind=dp), intent(out):: mag(n,3), ku(n,3), xyz(n,3)
        real(kind=dp):: thet, phi
        integer:: i,j,z, p=1 
        ! GENERATE A SET OF ALEATORY NUMBERS
        call srand(time())
        ! FILLING ARRAYS

        do i=1,10
            do j=1,10
                do z=1,10
                    XYZ(p,:) = (/i,j,z/)
                    p = p+1
                end do 
            end do 
        end do

        do i=1,n
            
            thet=RAND()*PI     ! polar angle (theta)
            phi=RAND()*2.*PI     ! azimutal angle (phi)
       
           ! initial random positions of nanoparticles
       
            !XYZ(i,1)=sin(thet)*cos(phi)
            !XYZ(i,2)=sin(thet)*sin(phi)
            !XYZ(i,3)=cos(thet)



            ! XYZ(i,1)=RAND()
            ! XYZ(i,2)=RAND()
            ! XYZ(i,3)=RAND()

            thet = pi*rand() ! Polar angle theta
            phi = 2.0*pi*rand() !Azimutal angle phi

            ! Initial random directions of nanoparticles
            mag(i,1) = sin(thet)*cos(phi)
            mag(i,2) = sin(thet)*sin(phi)
            mag(i,3) = cos(thet)

            thet = pi*rand() ! Polar angle theta
            phi = 2.0*pi*rand() !Azimutal angle phi

            ! Uniaxial random directions of nanoparticles
            ku(i,1) = sin(thet)*cos(phi)
            ku(i,2) = sin(thet)*sin(phi)
            ku(i,3) = cos(thet)
        end do 

    end subroutine

    ! --------------------------------------------------------------
    ! CALCULATING TOTAL MAGNETIC FIELD
    subroutine subr4(mag, vec_h, n,mag_total)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        real(kind=dp), intent(in):: mag(n,3), vec_h(3)
        integer, intent(in):: n
        real(kind=dp), intent(out):: mag_total 
        integer:: j
        ! FILLING mag_total
        mag_total = 0
        do j=1,n 
            mag_total = mag_total + dot_product(mag(j,:), vec_h)
        end do 

    end subroutine

    ! --------------------------------------------------------------

    ! MARKOV CHAIN
    subroutine subr5(mcs,n,mag,hext,vecH,dThet,ku,pi,magTotal)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        integer, intent(in):: mcs, n
        real(kind=dp), intent(in):: mag(n,3), vecH(3), ku(n,3), hext, dThet, pi
        real(kind=dp), intent(out):: magTotal
        integer:: j,k,randN
        real(kind=dp):: magT(n,3)

        ! DETERMINATING magTotal
        magT = mag
        do j=1, mcs
            do k=1, n 
                randN = nint(n*rand()) ! Random nanoparticle
                call subr6(vecH, hext, dThet, ku(randN,:), pi, magT(randN,:))
            end do 
        end do        
        call subr4(mag,vecH,n,magTotal)

    end subroutine

        ! --------------------------------------------------------------
    ! SETTING MAG AND MAGAUX 2
    subroutine subr6(vecH, hext, dThet, ku, pi, mag)

        integer, parameter :: dp = selected_real_kind(15, 307)
        ! VARIABLE DECLARATION
        real(kind=dp), intent(in):: vecH(3), hext, dThet, pi, ku(3)
        real(kind=dp), intent(inout):: mag(3)
        real(kind=dp):: ei, ej, nrj, t, magAux(3)
        ! SETTING mag VALUES
        ei = -0.5*(dot_product(ku,mag))**2 - hext*dot_product(mag,vecH)
        magAux(:) = mag
        call genedThet(magAux, dThet, pi) 
        ej = -0.5*(dot_product(ku,magAux(:)))**2 - hext*dot_product(magAux(:),vecH)
        t = 0.1
        if (ej <= ei) then
            mag = magAux(:)
        else 
            nrj = rand()
            if (nrj < exp(-(ej-ei)/t)) then 
                mag = magAux 
            end if   
        end if

    end subroutine

end module fortran