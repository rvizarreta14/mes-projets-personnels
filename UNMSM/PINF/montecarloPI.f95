program montecarloPI
implicit none
integer:: i
double precision:: pi

open(unit=10,file='datos.txt')

do i=50000, 1000000, 500

call calcularPi(i, pi)

write(*,'(a4,1x,i7,1x,a5,1x,f6.4)') "n = ", i, "pi = ", pi
write(10,*) i, pi
end do

! PARA GRAFICAR EN GNUPLOT
! set xlabel "# de datos"
! set ylabel "Valor de pi"
! set title "Determinando el valor de pi por el Metodo Montecarlo"
! plot "datos.txt"

end program montecarloPI

subroutine calcularPi(n, pi)

integer, intent(in):: n 
double precision, intent(out):: pi
double precision:: sumIn, sumOut, x, y
   
call random_seed

sumIn = 0.0
sumOut = 0.0

do i=1,n
    call random_number(x)
    call random_number(y)
    
    if(x**2+y**2<1.0) then
        sumIn = sumIn + 1.0
    else 
        sumOut = sumOut + 1.0
    end if
end do

pi = 4.0*(sumIn/(sumIn + sumOut))

end subroutine