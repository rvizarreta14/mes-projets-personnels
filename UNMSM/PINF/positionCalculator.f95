
! El presente programa permite calcular la velocidad en cualquier instante de
! tiempo para un objeto que se mueve con dos tipos de movimiento: para t=0 s el objeto
! inicia su movimiento con Vo = 2 m/s y se mantiene con un MRUV por 3 segundos hasta
! llegar a una V = 8 m/s, para posteriormente continuar su movimiento a velocidad constante.

program positionCalculator
implicit none
integer :: j, i = 0
real :: x, x2, c, d, a, t, t2, t3, v, vo, v2, posMRUV, posMRU, aceleration

print*, "Bienvenido a positionCalculator. El presente programa permite calcular la posición &
&en cualquier instante de tiempo de un objeto que se mueve con dos tipos de &
&movimiento: para t=0 s el objeto inicia su movimiento con velocidad inicial Vo &
&y se mantiene con un MRUV por 3 segundos hasta llegar a una velocidad V, para &
&posteriormente continuar su movimiento a velocidad constante (MRU) por 9 segundos más. Posteriormente el objeto &
& continua su movimiento con un MRUV de forma indefinida. Para comenzar, &
&digite la velocidad inicial Vo (m/s) del objeto."
read*, vo

print*, "Excelente, ahora ingrese la posición inicial (x) para t=0"
read*, x

print*, "Ingrese el tiempo (segundos) que el objeto permanecerá en movimiento con un MRUV"
read*, t 

print*, "Ingrese la velocidad que tendrá el cuerpo luego de estos ", t, " segundos"
read*, v

a = aceleration(v,vo,t)

print*, "La aceleración del objeto es de ", a, " m/s2"

print*, "Buen trabajo, ahora indica el tiempo que el objeto permanecerá en movimiento a velocidad constante de ", v, " m/s"
read*, t2

print*, "Genial, segun tus datos ingresados el cuerpo comenzará a moverse con MRUV nuevamente &
&luego de ", t+t2, " segundos luego de iniciado su movimiento"

print*, "Excelente, ya hemos culminado con el ingreso de data. Ya puedes calcular la &
&posición en un instante determinado. "

do while (i==0)
print*, "Por favor, indica el instante de tiempo t para el cual deseas determinar la posición"
read*, t3

if (t3>=0) then
    if (t3<=t) then
        d = posMRUV(x,vo,a,t3)
        i=1
    else if(t3<=(t+t2)) then
        d = posMRUV(x,vo,a,t) + posMRU(v,t,t3)
        i=1
    else if(t3>(t+t2)) then
        print*, "Indicar la velocidad final del cuerpo"
        read*, v2
        x2 = posMRUV(x,vo,a,t) + posMRU(v,0.0,t2) 
        d = posMRUV(x2,v,aceleration(v2,v,t3-t2-t),t3-t2-t)
        i=1
    end if
else 
    print*, "Tiempo no válido"     
end if

end do

!select case (t3)
!case (<= t)
!d = posMRUV(x,vo,a,t3)
!case (t::t2)
!d = posMRUV(x,vo,a,t) + posMRU(v,t,t3)
!case (>= t)
!print*, "Indicar la velocidad final del cuerpo"
!read*, v2
!x2 = posMRUV(x,vo,a,t) + posMRU(v,t,t2) 
!d = posMRUV(x2,v,aceleration(v2,v,t3-t2),t3-t2)
!end select

print*, "La posiciòn para t= ", t3, " seg es ", d, " m"

print*, "Se agrega la tabla de posición vs tiempo para los ", t3, "seg indicados"

do j=0, int(t3)
    c = real(j)
    if (c<=t) then
        d = posMRUV(x,vo,a,c)
    else if(c<=(t+t2)) then
        d = posMRUV(x,vo,a,t) + posMRU(v,t,c)
    else if(c>(t+t2)) then
        d = posMRUV(x2,v,aceleration(v2,v,c-t2-t),c-t2-t)
    end if
    print*, "Tiempo: ",j," seg, Posición: ",d," m"
end do

end program positionCalculator

real function posMRUV(x,vo,a,t)
real x,vo,a,t
posMRUV = x + vo*t + 0.5*a*t**2
return
end

real function posMRU(v,t0,t1)
real v,t1,t0 
posMRU = v*(t1-t0)
return 
end

real function aceleration(v,vo,t)
real v,vo,t 
aceleration = (v-vo)/t 
return 
end
