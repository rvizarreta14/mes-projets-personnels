program minimosCuadrados

    implicit none

    real, allocatable:: a(:,:), r(:,:)
    real:: m, b
    integer:: i, j, n

    ! LEYENDO DATOS DE TECLADO Y ESCRIBIÉNDOLOS EN UN ARCHIVO

    print*, "Ingrese la cantidad de puntos (x,y) a utilizar"
    read*, n

    allocate(a(n,2)) !Reservando memoria para el array
    open(unit=2,file='datos.txt')

    do i=1,n
        do j=1,2
        write(*,'(a21,i2,a1,i2,a1)') "Ingresar elemento a(", i, ",",j,")"
        read*, a(i,j) 
        end do
        write(2,*) a(i,1), a(i,2)
    end do

    close(2)

    ! LEYENDO DATOS DE UN ARCHIVO .txt Y GENERANDO LA REGRESIÓN
    
    open(unit=2,file='datos.txt')
    n = 0

    do 
        read(2,*,end=10)
        n = n + 1
    end do
    10 close (2)

    open(unit=2,file='datos.txt')

    allocate(r(n,2))

    do i=1,n
        read(2,*) r(i,1), r(i,2)
    end do
    
    close(2)

    call regresion(r,m,b, n)

    write(*,'(a3,1x,f6.2,1x,a3,1x,f6.2)') "y =", m, "x +",b

    end program minimosCuadrados

subroutine regresion(a, m, b, n)

    real, intent(in):: a(n,2)
    real, intent(out):: m,b
    real:: sumx = 0, sumy = 0, sumxy = 0, sumx2 = 0
    integer n, i

    do i=1,n 
        sumx = sumx + a(i,1)
        sumy = sumy + a(i,2)
        sumxy = sumxy + a(i,1)*a(i,2)
        sumx2 = sumx2 + a(i,1)**2
    end do
    
    b = (sumy*sumx2 - sumx*sumxy)/(n*sumx2 - sumx**2)
    m = (n*sumxy - sumx*sumy)/(n*sumx2 - sumx**2) 
    
end subroutine 