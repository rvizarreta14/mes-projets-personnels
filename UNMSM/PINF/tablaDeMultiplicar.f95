program tablaDeMultiplicar20x20
implicit none
integer, dimension(20,20):: tabla
integer:: i,j,n 

do
    print*, "Ingrese un número entero entre 2 y 20"
    read*, n 
    if(n>=2 .and. n<=20) exit
end do

do i=1,n
        do j=1,n
        tabla(i,j) = i*j            
        end do
end do 

print*, "Tabla de multiplicar del 1 hasta el ", n
do i=1,n
    print*, (tabla(i,j), j=1,n)
end do

end program tablaDeMultiplicar20x20