program movimientoParabolico
implicit none
real:: altura
real:: range, recorridoHoriz, vo
real, dimension(10000,5):: data, data2
integer:: i, j, k, m=0

print*, "Ingrese el valor de la velocidad de lanzamiento v0 menor a 100 m/s"
read*, vo

do i=5,85,5
    range = recorridoHoriz(vo,real(i))
    if(i<=45) then
        print*, "Ángulo rasante: ", i, "°, desplazamiento horizontal:", range," m"
    else 
        print*, "Ángulo por elevación: ", i, "°, desplazamiento horizontal: ", range," m"
    end if
end do

!GENERANDO GRÁFICA DE VELOCIDAD CTE Y ÁNGULO VARIABLE

do i=1, 10000
      k = 0
      data(i,1) = real(i)/10.0 !Posición
      do j= 2, 5
         k = k + 15
         if(real(i)/10.0 <= recorridoHoriz(vo, real(k))) then
            data(i,j) = altura(vo, real(k), real(i)/10.0)
            if(j==4) then
                m = m + 1 ! En j=4 el ángulo = 45°, donde se dispone del alcance horizontal máximo
            end if
         else 
            data(i,j) = 0.0 
         end if
     end do
end do

open(unit=10, file='data.txt')

do i=1,m
    write(10,*)(data(i,j), j=1,5)
end do

!Para obtener la gráfica en GNUPLOT:
! set xlabel "Posicion"
! set ylabel "Altura"
! set title "Trayectoria de un proyectil lanzado a una misma velocidad y angulo variable"
! plot 'data.txt' u 1:2 w l title '15 grados', 'data.txt' u 1:3 w l title '30 grados', 'data.txt' u 1:4 w l title '45 grados', 'data.txt' u 1:5 w l title '60 grados'


!GENERANDO GRÁFICA DE VELOCIDAD VARIABLE Y ÁNGULO CTE

m = 0
do i=1, 10000
    k = 0
    data2(i,1) = real(i)/10.0 !Posición
    do j= 2, 5
       k = k + 20
       if(real(i)/10.0 <= recorridoHoriz(real(k), 30.0)) then
          data2(i,j) = altura(real(k), 30.0, real(i)/10.0)
          if(j==5) then
              m = m + 1 !En j=5 la velocidad será 80 m/s, donde el alcance horizontal será el mayor
          end if
       else 
          data2(i,j) = 0.0 
       end if
   end do
end do

open(unit=12, file='data2.txt')

do i=1,m
  write(12,*)(data2(i,j), j=1,5)
end do

!Para obtener la gráfica en GNUPLOT:
! set xlabel "Posicion"
! set ylabel "Altura"
! set title "Trayectoria de un proyectil lanzado a una velocidad variable y angulo rasante de 30 grados"
! plot 'data2.txt' u 1:2 w l title '20 m/s', 'data2.txt' u 1:3 w l title '40 m/s', 'data2.txt' u 1:4 w l title '60 m/s', 'data2.txt' u 1:5 w l title '80 m/s'

end program movimientoParabolico

real function recorridoHoriz(vo, theta)
    real:: vo, theta, rad
    real, parameter:: g=-9.81
    recorridoHoriz = -2.0*(vo**2)*cos(rad(theta))*sin(rad(theta))/g
    return
end 

real function rad(theta)
    real:: theta
    real,  parameter :: pi  = 3.1415926535
    rad = theta*pi/180
    return
end

real function altura(vo, theta, x)
    real:: vo, theta, x
    real, parameter:: g=9.81
    altura = (tan(rad(theta)))*x - ((g)*(x**2))/((2.0)*(vo**2)*((cos(rad(theta))))**2)
    return
end
