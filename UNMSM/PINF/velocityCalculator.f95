
! El presente programa permite calcular la velocidad en cualquier instante de
! tiempo para un objeto que se mueve con dos tipos de movimiento: para t=0 s el objeto
! inicia su movimiento con Vo = 2 m/s y se mantiene con un MRUV por 3 segundos hasta
! llegar a una V = 8 m/s, para posteriormente continuar su movimiento a velocidad constante.

program velocityCalculator
implicit none
real :: x, d, a, t, t2, v, vo, posMRUV

print*, "Bienvenido a velocityCalculator. El presente programa permite calcular la velocidad &
&en cualquier instante de tiempo para un objeto que se mueve con dos tipos de &
&movimiento: para t=0 s el objeto inicia su movimiento con velocidad inicial Vo &
&y se mantiene con un MRUV por 3 segundos hasta llegar a una V = 8 m/s, para &
&posteriormente continuar su movimiento a velocidad constante (MRU). Para comenzar, &
&digite la velocidad inicial Vo (m/s) del objeto."
read*, vo

print*, "Excelente, ahora ingrese la posición inicial (x) para t=0"
read*, x

print*, "Ahora ingrese la velocidad final (m/s) a la que se estará moviendo el objeto"
read*, v 

print*, "Ingrese el tiempo (s) en el que el objeto empieza a moverse a tal velocidad"
read*, t 

a = (v-vo)/t 

print*, "La aceleración del objeto es de ", a, " m/s2"

print*, "Excelente, ya hemos culminado con el ingreso de data. Ya puedes calcular la &
&posición en un instante determinado. Por favor, indica el instante de tiempo t para el cual &
&deseas determinar la posición"
read*, t2

if(t2>=3) then
    d = posMRUV(x,vo,a,3.0) + v*(t2-3)
    !d = x + vo*3 + 0.5*a*3**2 + v*(t2-3)
else 
    d = posMRUV(x,vo,a,t2)
    !d = x + vo*t2 + 0.5*a*t2**2
end if

print*, "La posiciòn para t= ", t2, " es ", d, " m"

end program velocityCalculator

real function posMRUV(x,vo,a,t)
real x,vo,a,t
posMRUV = x + vo*t + 0.5*a*t**2
return
end