program cosenoSeries
implicit none
integer:: i = 0
integer:: factorial
double precision:: x, cosen=0.0d0, n=1.0d0 !Brinda 15 cifras significativas

print*, "Ingrese el valor (en radianes) cuyo coseno se desea calcular"
read*, x 

do  
    cosen = cosen + n
    i = i+1
    !n = ((-1)**i)*((x)**(2*i))/(factorial(2*i))
    n = n*(-x**2)/(2*i*(2*i-1))
    if(cosen + n == cosen) exit
end do

print*, "El valor del Cos(", x,") es: ", cosen
print*, "EL valor correcto es: ", COS(x)

end program cosenoSeries

! MÉTODO NO UTILIZADO, APARENTEMENTE CUANDO EL FACTORIAL
! SUPERA EL VALOR DE 2147483647 RETORNA CERO,
! LO CUAL HACE EL BUCLE INFINITO
integer function factorial(k)
    integer:: i,k
    factorial = 1
    if(k==0) then
        factorial = 1
    else 
        do i=1,k 
            factorial = factorial*i
        end do 
    end if 
    return
end