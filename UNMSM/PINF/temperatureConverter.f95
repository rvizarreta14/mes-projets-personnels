program temperatureConverter
implicit none
integer:: t 
real:: c

print*, "Indicar un valor de temperatura en °F en el rango entre -50°F y 150°F inclusive"
read*, t

if(t>-50 .and. t<=150) then
    c = (5.0/9.0)*(REAL(t)-32)
    print*, t, "°F equvale a ", c,"°C"
else 
    print*, "Temperatura fuera del rango permisible"
end if

end program temperatureConverter