program leapYear
implicit none
integer:: y

print*, "Ingrese el año a evaluar"
read*, y 

if(MOD(y,400)==0) then
    print*, "El año ", y, " es bisiesto"
else if(MOD(y,100)==0) then
    print*, "El año ", y, " no es bisiesto"
else if(MOD(y,4)==0) then
    print*, "El año ", y, " es bisiesto"
else 
    print*, "El año ", y, " no es un año bisiesto"
end if

end program leapYear