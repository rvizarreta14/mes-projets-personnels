program LennardJonesExample
implicit none
real:: sep, pot, sigma=3.405, epsilon=0.010323 

call equilibrium(sigma, epsilon, sep, pot)

write(*,'(a32,x,f8.6,x,a35,x,f9.6,x,a2)') "La separaciòn de equilibrio es", sep, "A y la energìa correspondiente es", pot, "eV"

end program LennardJonesExample

subroutine equilibrium(sigma, epsilon, sep, pot)

real, intent(in):: sigma, epsilon
real, intent(out):: sep, pot

sep = sigma*2**(1.0/6.0)
pot = 4.0*epsilon*((sigma/sep)**12-(sigma/sep)**6)

end subroutine