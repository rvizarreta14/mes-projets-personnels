program gravitationalAcceleration
implicit none
real, allocatable:: datos(:,:)
real:: calcularGravedad, g, r, coefCorrelacion
integer:: n,i,j

print*, "Ingrese la cantidad de datos (L(m),T(s)) a utilizar"
read*, n

allocate(datos(n,2))

do i=1,n
    do j=1,2
        select case(j)
            case(1)
                print*, "Ingrese la longitud (m) ", i
                read*, datos(i,j)
            case(2)
                print*, "Ingrese el periodo (s) ", i
                read*, datos(i,j)
            end select 
    end do
end do

g = calcularGravedad(datos,n)
print*, "La gravedad resultande es ", g, " m/s2"

r = coefCorrelacion(datos,n)
print*, "El coeficiente de correlaciòn es ", r

end program gravitationalAcceleration


real function calcularGravedad(datos,n)
real:: sum=0, datos(n,2), L, T
real, parameter:: pi=3.14159265359 
integer:: n,i

do i=1,n 
    L= datos(i,1)
    T= datos(i,2)
    sum = sum + (4*L*pi**2)/T**2
end do

calcularGravedad = sum/n

return 
end


real function coefCorrelacion(datos,n)

real:: datos(n,2)
real:: sumx = 0, sumy = 0, sumxy = 0, sumx2 = 0, sumy2 = 0
integer n, i

do i=1,n 
    sumx = sumx + datos(i,1)
    sumy = sumy + datos(i,2)
    sumxy = sumxy + datos(i,1)*datos(i,2)
    sumx2 = sumx2 + datos(i,1)**2
    sumy2 = sumy2 + datos(i,2)**2
end do

coefCorrelacion = (n*sumxy-sumx*sumy)/sqrt((n*sumx2-sumx**2)*(n*sumy2-sumy**2))
 
return
end  
