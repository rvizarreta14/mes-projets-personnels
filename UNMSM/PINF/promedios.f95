program promedios

implicit none
character(len=10):: headers(1,8), names(5,1)
real:: notas(5,6), average(5,1)
integer:: i

open(unit=2, file='notas.txt')
    read(2,*) headers(1,1), headers(1,2), headers(1,3), headers(1,4), headers(1,5), headers(1,6), headers(1,7), headers(1,8)
    do i=1,5
        read(2,*) names(i,1), notas(i,1), notas(i,2), notas(i,3), notas(i,4), notas(i,5), notas(i,6)
    end do
close(2)

call prom(notas, average)

write(*,'(a11,2x,a3)') headers(1,1), headers(1,8)
print*, "------------------"
do i=1,5
    write(*,'(a11,2x,f5.2)') names(i,1), average(i,1)
end do

end program promedios

subroutine prom(notas, average)

real, intent(in):: notas(5,6)
real, intent(out):: average(5,1)
real:: sum
integer:: i,j

do i=1,5
    sum = 0
    do j=1,6
        sum = sum + notas(i,j)
    end do
    average(i,1) = sum/6
end do

end subroutine