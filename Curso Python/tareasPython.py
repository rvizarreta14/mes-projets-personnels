#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 22:21:25 2019

@author: vizi
"""

"""
 Crear un diccionario cuyas claves sean los tres primeros dias de la semana y los 
 valores sean la posicion en la semana de dicho dia
"""

"""
 De dicho diccionario, convertir todas las claves a mayúsculas
"""
#%%
# CREANDO UN DICCIONARIO
dicci = {
        "lunes": 1,
        "martes": 2,
        "miercoles": 3,
        }

#%%

dicci["LUNES"] = dicci.pop("lunes")
dicci["MARTES"] = dicci.pop("martes")
dicci["MIERCOLES"] = dicci.pop("miercoles")
print(dicci)


#%%

# Dado el diccionario:
dias_semana = {"lunes":1,
               "martes":2,
               "miercoles": 3,
               "jueves": 4,
               "viernes": 5, 
               "sabado": 6,
               "domingo": 7
               }

"""
De dicho diccionario, convertir todas las claves a mayúsculas, usando un bucle
"""

"""
ESTO NO FUNCIONA, NO PUEDE CAMBIARSE EL TAMAÑO DE UN DICCIONARIO EN UN FOR

for dia in dias_semana:
    if dia == "lunes":
        dias_semana["LUNES"] = dias_semana.pop("lunes")
    elif dia == "martes":
        dias_semana["MARTES"] = dias_semana.pop("martes")     
    elif dia == "miercoles":
        dias_semana["MIERCOLES"] = dias_semana.pop("miercoles") 
    elif dia == "jueves":
        dias_semana["JUEVES"] = dias_semana.pop("jueves") 
    elif dia == "viernes":
        dias_semana["VIERNES"] = dias_semana.pop("viernes") 
    elif dia == "sabado":
        dias_semana["SABADO"] = dias_semana.pop("sabado") 
    else:
        dias_semana["DOMINGO"] = dias_semana.pop("domingo") 


print(dias_semana)
 
"""

dias_semana2 = {}
for dia, valor in dias_semana.items():
    dias_semana2[dia.upper()] = valor

dias_semana = dias_semana2
print(dias_semana)
 
#%%

"""
 De dicho diccionario, crear una lista de los dias de la semana que contengan 
 la letra "O"
"""

dias_con_o = []
for dia in dias_semana:
        if "O" in dia:
            dias_con_o.append(dia)
            
print(dias_con_o)

#%%

"""
 Crear funcion resta que resta 2 numeros y devuelve el resultado
"""

def resta(a,b):
    return a-b

print(resta(8,3))

#%%

""" Crar una funcion que acepta 3 argumentos, 2 numeros y un string. Si el string es
 "suma", devolver la suma de los dos numeros, si el string es "resta" devolver
 la resta
"""

def operation(a,b):
   while 1:
        text = input("Indique la opción de su preferencia: suma | resta --> ")

        if text=="suma":
            return a+b
            
        elif text=="resta":
            return a-b
            
        else:
            print("Petición no encontrada, intente denuevo")

resultado = operation(9,2)
print(resultado)

#%%

"""
Crear una funcion que pregunte al usuario una frase y devuelva dicha frase con
palabras en orden inverso. Por ejemplo, si el usuario dice "la lluvia en Sevilla"
la funcion devolverà "Sevilla en lluvia la"
"""
text = input("Ingresar frase: ")

def inversion(text):
    words = text.split(" ")
    text2 = []
    for i in range(0,len(words),1):
        text2.append(words[len(words)-1-i])
    return " ".join(text2)

print(inversion(text))

#%%
# OTRA FORMA PARA EL ÚLTIMO EJERCICIO

def invertir(text=input("Ingresar frase: ")):
    words = text.split(" ")
    text2 = words[::-1]
    print((" ").join(text2))

invertir()
        
#%%

"""
Crear una función que detecte si una palabra o frase es palindromo (un palíndromo
es aquella palabra o frase que se lee de igual forma de un sentido u otro)
"""
text = input("Ingresar palabra: ")

def inversion2(text):
    text2 = []
    for i in range(0,len(text),1):
        text2.append(text[len(text)-1-i])

    if text == "".join(text2):
        print("Es un palindromo!!")
    else: 
        print("No es un palindromo :,(")
        
inversion2(text)

#%%
# OTRA FORMA PARA EL ÚLTIMO EJERCICIO

def inversion3(text=input("Ingresar palabra: ")):
    text2 = text[::-1]     
    if text == text2:
        print("Es un palindromo!!")
    else: 
        print("No es un palindromo :,(")
inversion3()


#%%

"""
 Crear una funcion que dada una lista de listas, nos devuelva una lista simple (es decir, sin ninguna lista dentro)

por ejemplo, si a dicha funcion le pasamos el argumento

lista_nesteada = [
        [1,2,3],
        [4,5.6,7],
        [8,9]
]

la funcion nos devolveria la lista [1,2,3,4,5,6,7,8,9]

"""

lista = [
    ['renzo','pelusa','nelly'],
    [1,2,3,4,845],
    [23, 'webon de prado', 14, 'Ovelar vuelve :('],
]
    
def unification(list):
    list2 = []
    for i in range(0,len(list),1):
        for j in range(0,len(list[i]),1):
            list2.append((list[i])[j])
    print(list2)
    
unification(lista)

#%%

# OTRA FORMA PARA EL ÚLTIMO EJERCICIO

def unificacion(list):
    list2 = []
    for lista in list:
        for elemento in lista:
            list2.append(elemento)
    
    print(list2)

unificacion(lista)

#%%
"""
 Crear funcion lambda que convierte un string a minusculas
"""

minuscula = lambda mayus: mayus.lower()
print(minuscula(input("Ingrese un texto en mayúscula: ")))

#%%

#VOLVIENDO A ESCRIBIR EL EJEMPLO DE CLASE

class Vehiculo:

    def __init__(self, modelo, velocidad_maxima, color="negro"):
        self.color = color
        self.modelo = modelo
        self.velocidad_maxima = velocidad_maxima
        self.velocidad = 0 #el coche empieza parado
        
    def describir(self):
        descripcion = "Vehiculo Modelo:{}. Color {}. Velocidad máxima: {}".format(
                self.modelo, self.color, self.velocidad_maxima)
        return descripcion
    
    # El metodo __repr__ es un metodo mágico que se usa cuando queremos representar algo (con el metodo print)
    def __repr__(self):
        return self.describir()

    def describir_estado(self):
        if self.velocidad == 0:
            print("El vehiculo está parado")
        elif self.velocidad > 0:
            print("El vehiculo va a {} kilómetros por hora".format(self.velocidad))
        else:
            print("El vehiculo va marcha atrás {} a kilómetros por hora".format(self.velocidad))
            
    def girar_izquierda(self):
        print("Girando a la izquierda")

    def girar_derecha(self):
        print("Girando a la derecha")

    def acelerar(self, diferencia_velocidad):
        print("Acelerando {} km/h".format(diferencia_velocidad))
        # abs devuelve un numero positivo si es negativo
        self.velocidad += abs(diferencia_velocidad)
        # min devuelve el valor minimo de una lista de números
        self.velocidad = min(self.velocidad, self.velocidad_maxima)

    def frenar(self, diferencia_velocidad):
        print("Frenando {} km/h".format(diferencia_velocidad))
        self.velocidad -= abs(diferencia_velocidad)
        # max nos devuelve el máximo valor de una lista de números
        self.velocidad = max(self.velocidad, -5)

"""
******************************************************************************

EJERCICIO

Crear una clase Taxi, que herede de Vehiculo, y que pueda cobrarnos un trayecto
Tiene que tener un atributo "distancia_recorrida", y tres metodos adicionales:
  - metodo "cobrar", donde se imprime la cantidad a cobrar (3 euro por kilometro)
  - metodo "añadir_distancia", donde se suma a la distancia recorrida un numero de kilometros
  - metodo "añadir_tiempo", donde dado un tiempo (en horas) se añade distancia en función de la velocidad
******************************************************************************
"""

class Taxi(Vehiculo):
    
    def __init__(self, modelo, velocidad_maxima, tarifa):
        self.color = "Amarillo"
        self.modelo = modelo
        self.velocidad = 0
        self.velocidad_maxima = velocidad_maxima
        self.recorrido = 0
        self.tarifa = tarifa
        
    def cobrar(self):
        print("El monto a pagar es: {} euros".format(self.tarifa*self.recorrido))
        print("El recorrido total fue de {} km".format(self.recorrido))
    
    def añadirDist(self, distancia):
        self.recorrido += distancia
        
    def añadirTime(self, tiempo):
        #self.recorrido += tiempo*self.velocidad
        self.añadirDist(tiempo*self.velocidad)
        
viziTaxi = Taxi("Tesla", 40, 20)
viziTaxi.acelerar(20)
viziTaxi.añadirTime(0.33)
viziTaxi.girar_derecha()
viziTaxi.acelerar(10)
viziTaxi.añadirTime(1)
viziTaxi.girar_derecha()
viziTaxi.frenar(20)
viziTaxi.añadirTime(4)
viziTaxi.cobrar()

#%%

"""
******************************************************************************

EJERCICIO

Crear una clase Parking, donde puedan aparcar un limite determinado de Vehiculos 
(solo Vehiculos!)
y donde se puede validar si un vehiculo esta aparcado o no
******************************************************************************
"""

class Parking:
    
    def __init__(self, nombre, capacidad):
        self.nombre = nombre
        self.capacidad = capacidad
        self.aparcados = []
        
    def park(self, vehiculo):
        
        if not type(vehiculo) == Taxi:
            if vehiculo not in self.aparcados and len(self.aparcados)<self.capacidad:
                self.aparcados.append(vehiculo)
            else:
                print("No hay más capacidad o ya está estacionado aquí")
        else:            
            print("No se permiten taxis, fuera!")
        
    def out(self, vehiculo):
        
        if not type(vehiculo) == Taxi:
            if vehiculo not in self.aparcados:
                print("El vehículo no está estacionado aquí")
            else:
                self.aparcados.pop(self.aparcados.index(vehiculo))
        else:            
            print("No hay taxis aquí, pesado!")
            
    def ubicacion(self, vehiculo):
        
        if not type(vehiculo) == Taxi:
            if vehiculo not in self.aparcados:
                print("El vehículo no está estacionado aquí")
            else:
                print("Tu vehículo está estacionado aquí")
        else:            
            print("No sé dónde estará tu taxi, pero aquí no está. Pesado!")
            
    def utilizacion(self):
        
        utilization = len(self.aparcados)*100/self.capacidad
        print("La utilizacion es del {}%".format(utilization))
        
viziTaxi = Taxi("Tesla", 40, 20)
viziTaxi2 = Taxi("Tesla", 40, 20)
viziTaxi3 = Taxi("Tesla", 40, 20)
auto1 = Vehiculo("Nissan1", 20)
auto2 = Vehiculo("Nissan2", 20)
auto3 = Vehiculo("Nissan3", 20)
viziParking = Parking("viziParks", 2)
viziParking.park(viziTaxi)
viziParking.park(auto1)
viziParking.utilizacion()
viziParking.park(auto2)
viziParking.utilizacion()
viziParking.ubicacion(auto3)
viziParking.ubicacion(viziTaxi2)
viziParking.park(auto3)
viziParking.out(auto1)
viziParking.park(auto3)
viziParking.ubicacion(auto1)
viziParking.aparcados

#%%

"""
 Convertir la lista de pokemon_entrenadores en una lista de diccionarios con las 
claves "entrenador" y "pokemon"

[
{"entrenador": "Ash", "pokemon": "Pikachu"},
{"entrenador":"Ash", "pokemon": "Nidorian"},
...
]
"""

pokemon_entrenadores_lista = [
         ['Ash', 'Nidorian'],
         ['Ash', 'Charmander'],
         ['Ash', 'Jigglypuff'],
         ['Ash', 'Rattata'],
         ['Ash', 'Pikachu'],
         ['Ash', 'Pidgey'],
         ['Misty', 'Pikachu'],
         ['Misty', 'Squirtle'],
         ['Misty', 'Jigglypuff'],
         ['Misty', 'Rattata'],
         ['Brock', 'Nidorian'],
         ['Brock', 'Charmander'],
         ['Brock', 'Jigglypuff']
]
"""
entrenadores_lista = []

for registro in pokemon_entrenadores_lista:
        dicci_elm = {}
        dicci_elm['entrenador'] = registro[0] 
        dicci_elm['pokemon'] = registro[1]
        entrenadores_lista.append(dicci_elm)
        
print(entrenadores_lista)
"""

entrenadores_dicci = []
for entrenador, pokemon in pokemon_entrenadores_lista :
    entrenadores_dicci.append({'entrenador': entrenador, 'pokemon': pokemon})
entrenadores_dicci
        
#%%

"""
Hacer una funcion que toma una frase y devuelve las 5 letras mas comunes 
"""
from collections import Counter

def letritas(frase):
    recuento = Counter(frase.replace(" ","")).most_common(5)
    print(recuento)
    
letritas("vive la vida y no dejes que la vida te viva")
        
#%%

"""
Crear una función que, dados dos listas de elementos, nos devuelva su coeficiente de jaccard.
 El coeficiente de Jaccard es una medida de similaridad entre dos grupos y se define
 como el número de elementos en los dos grupos dividido entre el número de elementos 
 en uno u otro grupo
"""
"""
lista1 = [1,2,3,4,5,6,7,8]
lista2 = [3,4,5,6,7,8,9,10]

def jaccard(lista1, lista2):
    comunes = 0
    no_comunes = 0
    for elemento1 in lista1:
        if elemento1 in lista2:
            comunes += 1
        else: 
            no_comunes += 1
    return comunes/(len(lista1)+len(lista2)-comunes)   

jaccard(lista1,lista2)
"""
lista1 = [1,2,3,4,5,6,7,8]
lista2 = [3,4,5,6,7,8,9,10]

def jaccard(lista1, lista2):
    union = len(set(lista1).union(set(lista2)))
    interseccion = len(set(lista1).intersection(set(lista2)))
    return  "El coef. de Jaccard es {:.2f}".format(interseccion/union)

jaccard(lista1,lista2)

#%%

dicci = {
"nombre": ["Antonio", "Miguel", "Julian", "Andres"],
"edad": [45, 40, 22, 34],
"ciudad": ["Murcia", "Almería", "Barcelona", "Madrid"]
}  
claves = list(dicci.keys())
print(claves)
#nombres = []
#for j in range(0,4,1):
#    for i in range(0,len(claves),1):
#        for lista in dicci[claves[i]]:
#            nombres.append(lista[j])




