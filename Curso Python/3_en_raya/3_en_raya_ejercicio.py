# -*- coding: utf-8 -*-
"""

Ejercicio. 3 en Raya

Este ejercicio nos va a ayudar a afianzar los conocimientos adquiridos sobre flujo
de control, y estructuras de datos

Vamos a hacer un programa que nos permita jugar al 3 en raya desde la terminal


Básicamente el panel de 3 en raya son 3 listas dentro de otra

tablero = [
        [" ", " ", " "],
        [" ", " ", " "],
        [" ", " ", " "]
]

por ejemplo, si queremos ver cual es el estado de la casilla de arriba a la
izquerda, podemos acceder haciendo tablero[[0,0]]

Tenemos a 2 jugadores, que turno por turno iran eligiendo coordenadas en el tablero
y poniendo una ficha, una "X" para el jugador 1 y un circulo "O" para el jugador 2


El juego tendrá que validar que las casillas que elijan los jugadores no estén ya
ocupadas por una ficha (es decir, solo se pueden actualizar casillas que sean 0)

"""


from collections import deque

"""
Un deque es una lista donde se pueden insertar elementos por la izquierda y por la
derecha.

Tiene un método llamado rotate que simplemente "rota" los elementos.
Es decir, rotate() pone el ultimo elemento del deque el primero, el primer
elemento el segundo etcétera.
"""

turno = deque(["O", "X"])


tablero = [
    [" ", " ", " "],
    [" ", " ", " "],
    [" ", " ", " "]
]

# Podemos crear una funcion, cambiar_turno() que rota el deque de turnos y devuelve
# el jugador activo


def cambiar_turno():
    turno.rotate()
    return turno[0]

#%%
from collections import deque, Counter

def main():
    
    tablero = [
        [" ", " ", " "],
        [" ", " ", " "],
        [" ", " ", " "]
    ]
    
    turno = deque(['0','x'])
    print('Let the game begin!')
    
    def change():
        turno.rotate()
        return turno[0]    
    
    state = 1
    while state == 1:
        jugador = change()
        [print(fila) for fila in tablero]
        
        while 1:
            
            try:        
                positionF, positionC = input('Jugador "{}", escoger posición fila, columna entre las filas 1-3 y las columnas 1-3: '.format(jugador)).split(',')
                   
                if int(positionF) > 0 and int(positionF) < 4 and int(positionC) > 0 and int(positionC) < 4:
                    if tablero[int(positionF)-1][int(positionC)-1] == " ":
                        break             
                    else:
                        print("Posición ya ocupada, guarro")           
                else:
                    print("Valor(es) fuera del rango")
    
            except ValueError:
                print("Hay valor(es) no admitido(s) o información incompleta")
                
        tablero[int(positionF)-1][int(positionC)-1] = jugador
        
        #VALIDANDO FILAS Y COLUMNAS
        for i in range(3):
               
            columna = []
            
            for fila in tablero:
                if Counter(fila)[jugador] == 3:
                    print('Felicidades jugador "{}", ganaste1!'.format(jugador))
                    state = 0
                    break
                
                columna.append(fila[i])
                if Counter(columna)[jugador] == 3:
                    print('Felicidades jugador "{}", ganaste2!'.format(jugador))
                    state = 0
                    break 
            
            #SALIR SI YA HUBO GANADOR
            if state == 0:
                break
            
        #SALIR SI YA HUBO GANADOR
        if state == 0:
            break      
                
        #VALIDANDO DIAGONALES
        if tablero[1][1] != " ": 
            if tablero[0][0] == tablero[1][1] == tablero[2][2] or tablero[2][0] == tablero[1][1] == tablero[0][2]:
                print('Felicidades jugador "{}", ganaste3!'.format(jugador))
                state = 0
                break 
    
    [print(fila) for fila in tablero]      
    if state == 1:
        print('Empate!')
                
if __name__ == "__main__":
    main()
                

