# -*- coding: utf-8 -*-
"""
En este ejemplo vamos a hacer una aplicacion que lea los archivos de nombres más
populares de recien nacidos del INE, accesibles en este link
http://www.ine.es/dyngs/INEbase/es/operacion.htm?c=Estadistica_C&cid=1254736177009&menu=resultados&secc=1254736195498&idp=1254734710990

En concreto, esta aplicación hará lo siguiente.

- Tomar como argumento un nombre propio, por ejemplo "python nombres_bebes manuel"
- Leer un conjunto de archivos conteniendo los nombres más populares de niños en España por Año.
  Cada archivo es un archivo con el formato "nomnacXX.csv"
  donde XX es un año del 02 al 16 (2002 a 2016)
- Para el nombre indicado, imprimir cuantos años ha estado entre los nombres más populares

@author: manugarri
"""

#%%
import os
import argparse
from pathlib import Path

def parsear():
    parser = argparse.ArgumentParser(description = "Esta aplicación te permitirá determinar los años de popularidad que \
                                     un nombre de bebé ha tenido en los últimos 15 años")
    parser.add_argument('nombre', help = 'Indica un nombre no nulo para analizar')  
        
    argumentos = parser.parse_args()
    return argumentos
    
def validarNombre(nombre):
        
        user_input = argumentos.nombre.upper()
        if len(user_input.strip()) != 0:
            return True
        else:
            print('Nombre vacío')
            return False

def main(argumentos):
    
    # CÓDIGO PRINCIPAL
    user_input = argumentos.nombre.upper()
    carpeta = Path('/home/vizi/Desktop/nombres_bebes/data')
    files = os.listdir(carpeta)
    count = 0
    
    for file in files:
        names_in_file = []
        archivo = carpeta / file
        with open(archivo) as fname:
            lineas = fname.readlines()
        for doblete in [linea.strip('\n') for linea in lineas]:
            for i in range(2):
                names_in_file.append(doblete.split(',')[i])
            
        for name in names_in_file:
            if name == user_input:
                count += 1
          
    print('El nombre {} ha sido el nombre más popular por {} años'.format(argumentos.nombre, count))
    
if __name__ == '__main__':
    while 1:
        argumentos = parsear()
        if validarNombre(argumentos.nombre):
            break
    print('Argumentos del script: ', argumentos)
    main(argumentos)